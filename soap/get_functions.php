<pre>
<?php

require './soap_config.php';


$client = new SoapClient(null, array('location' => $soap_location,
    'uri' => $soap_uri,
    'trace' => 1,
    'stream_context' => stream_context_create(array('ssl' => array('verify_peer' => false, 'verify_peer_name' => false))),
    'exceptions' => 1));


try {
    if ($session_id = $client->login($soap_username, $soap_password)) {
        echo 'Logged successfull. Session ID:' . $session_id . '<br />';
    }

    //* Set the function parameters.

    $record_record = $client->get_function_list($session_id);

    print_r($record_record);
    echo "<br>";

    if ($client->logout($session_id)) {
        echo 'Logged out.<br />';
    }


} catch (SoapFault $e) {
    echo $client->__getLastResponse();
    die('SOAP Error: ' . $e->getMessage());
}

?>
</pre>