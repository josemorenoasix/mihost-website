<pre>
<?php
// prevent XSS
$_POST = filter_input_array(INPUT_POST, FILTER_SANITIZE_STRING);
session_start();

if (isset($_POST['cartdomain'])) {
    $_SESSION['cart']['domain'] = $_POST['cartdomain'];
    header('Location: ./index.php#domain');
}

if (isset($_POST['cartplan'])) {
    $_SESSION['cart']['plan'] = $_POST['cartplan'];
    header('Location: ./index.php#plans');
}

print_r($_SESSION);
print_r($_SERVER);
?>


