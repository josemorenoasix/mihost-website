<!DOCTYPE html>
<html lang="es">

<!-- Head tag -->
<head>
    <meta charset="UTF-8">
    <meta name="description" content="">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->

    <!-- Title  -->
    <title>miHost - Checkout</title>

    <!-- skeuocard includes (card number checker) -->
    <link rel="stylesheet" href="./src/skeuocard/styles/skeuocard.reset.css"/>
    <link rel="stylesheet" href="./src/skeuocard/styles/skeuocard.css"/>
    <link rel="stylesheet" href="./src/skeuocard/styles/demo.css">
    <script src="./src/skeuocard/javascripts/vendor/cssua.min.js"></script>

    <!-- Favicon  -->
    <link rel="shortcut icon" href="img/core-img/favicon.ico">

    <!-- ::::::::::::::::::: All CSS Files ::::::::::::::::::: -->

    <!-- Style css -->
    <link rel="stylesheet" href="style.css">

    <!-- Responsive css -->
    <link rel="stylesheet" href="css/responsive.css">

    <!--[if IE]>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/html5shiv/3.7.3/html5shiv.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->


</head>

<!-- Body tag -->
<body>
<!-- ::::::::::::::::::: include login.php ::::::::::::::::::: -->
<?php include("./php/include/login.php"); ?>
<!-- ::::::::::::::::::: Header End ::::::::::::::::::: -->

<!-- ::::::::::::::::::: include header.php ::::::::::::::::::: -->
<?php include("./php/include/header.php"); ?>
<!-- ::::::::::::::::::: Header End ::::::::::::::::::: -->

<!-- ::::::::::::::::::: Breadcumb area start ::::::::::::::::::: -->
<section class="breadcumb_area">
    <div class="container">
        <div class="row">
            <div class="col-xs-12">
                <div class="breadcumb_section">
                    <!-- Breadcumb page title start -->
                    <div class="page_title">
                        <h3>Checkout</h3>
                    </div>
                    <!-- Breadcumb start -->
                    <ol class="breadcrumb">
                        <li><a href="index.php">Home</a></li>
                        <li class="active">Checkout</li>
                    </ol>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- ::::::::::::::::::: Breadcumb area End ::::::::::::::::::: -->

<!-- ::::::::::::::::::: Checkout area start ::::::::::::::::::: -->
<section class="checkout_page section_padding_100_70">
    <div class="container">
        <form action="signup.php" method="post">
            <div class="row">
                <div class="col-md-6 col-xs-12">
                    <!-- Billing Address -->
                    <div class="checkbox-form">
                        <h3>Detalles </h3>
                        <div class="row">
                            <!-- Single Address Field -->
                            <div class="col-sm-6">
                                <div class="form-field">
                                    <input name="contactname" placeholder="Nombre de contacto" type="text" required>
                                </div>
                            </div>
                            <!-- Single Address Field -->
                            <div class="col-sm-6">
                                <div class="form-field">
                                    <input name="companyname" placeholder="Compañia (Opcional)" type="text">
                                </div>
                            </div>
                            <!-- Single Address Field -->
                            <div class="col-md-12">
                                <div class="form-field">
                                    <input name="username" placeholder="Nombre de usuario" type="text" required>
                                </div>
                            </div>
                            <!-- Single Address Field -->
                            <div class="col-md-8">
                                <div class="form-field">
                                    <input name="email" placeholder="Direccion Email" type="email" required>
                                </div>
                            </div>
                            <!-- Single Address Field -->
                            <div class="col-md-4">
                                <div class="form-field">
                                    <input name="telephone" placeholder="Telefono" type="tel">
                                </div>
                            </div>
                            <!-- Single Address Field -->
                            <div class="col-md-12">
                                <div class="country">
                                    <select name="country">
                                        <option value="ES">España</option>
                                    </select>
                                </div>
                            </div>
                            <!-- Single Address Field -->
                            <div class="col-xs-6">
                                <div class="form-field">
                                    <input name="province" placeholder="Provincia" type="text">
                                </div>
                            </div>
                            <!-- Single Address Field -->
                            <div class="col-xs-6">
                                <div class="form-field">
                                    <input name="city" placeholder="Ciudad" type="text">
                                </div>
                            </div>
                            <!-- Single Address Field -->
                            <div class="col-xs-9">
                                <div class="form-field">
                                    <input name="street" placeholder="Direccion" type="text">
                                </div>
                            </div>
                            <!-- Single Address Field -->
                            <div class="col-sm-3">
                                <div class="form-field">
                                    <input name="zip" placeholder="CP" type="number">
                                </div>
                            </div>
                            <!-- Single Address Field -->
                            <div class="col-md-5">
                                <div class="form-field">
                                    <label>ID unico de cliente.</label>
                                    <input name="customernumber" value="<?php echo uniqid($prefix = "C") ?>" type="text"
                                           readonly="yes">
                                </div>
                            </div>
                            <div class="col-md-7">
                                <div class="form-field">
                                    <label>Elige una clave de acceso.</label>
                                    <input name="password" placeholder="password" type="password" required>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-6 col-xs-12">
                    <!-- payment method accordion -->
                    <div class="checkbox-form">
                        <h3>Método de pago</h3>
                        <div class="payment_method">
                            <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
                                <!-- Single payment method -->
                                <div class="panel panel-default">
                                    <div class="panel-heading" role="tab" id="One">
                                        <h4 class="panel-title">
                                            <a class="collapsed" role="button" data-toggle="collapse"
                                               data-parent="#accordion" href="#collapse_one" aria-expanded="false"
                                               aria-controls="collapse_one">Pago con tarjeta</a>
                                        </h4>
                                    </div>
                                    <div aria-expanded="false" id="collapse_one" class="panel-collapse collapse in"
                                         role="tabpanel" aria-labelledby="One">
                                        <div class="panel-body">
                                            <div class="credit-card-input no-js" id="skeuocard">
                                                <p class="no-support-warning">
                                                    Tu navegador no soporta el nuevo formulario
                                                </p>
                                                <label for="cc_type">Card Type</label>
                                                <select name="cc_type">
                                                    <option value="">...</option>
                                                    <option value="visa">Visa</option>
                                                    <option value="discover">Discover</option>
                                                    <option value="mastercard">MasterCard</option>
                                                    <option value="maestro">Maestro</option>
                                                    <option value="jcb">JCB</option>
                                                    <option value="unionpay">China UnionPay</option>
                                                    <option value="amex">American Express</option>
                                                    <option value="dinersclubintl">Diners Club</option>
                                                </select>
                                                <label for="cc_number">Card Number</label>
                                                <input type="text" name="cc_number" id="cc_number"
                                                       placeholder="XXXX XXXX XXXX XXXX" maxlength="19" size="19">
                                                <label for="cc_exp_month">Expiration Month</label>
                                                <input type="text" name="cc_exp_month" id="cc_exp_month"
                                                       placeholder="00">
                                                <label for="cc_exp_year">Expiration Year</label>
                                                <input type="text" name="cc_exp_year" id="cc_exp_year" placeholder="00">
                                                <input type="text" name="cc_exp_year" id="cc_exp_year" placeholder="00"><label
                                                        for="cc_name">Cardholder's Name</label>
                                                <input type="text" name="cc_name" id="cc_name" placeholder="">
                                                <label for="cc_cvc">Card Validation Code</label>
                                                <input type="text" name="cc_cvc" id="cc_cvc" placeholder="123"
                                                       maxlength="3" size="3">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <!-- Single payment method -->
                                <div class="panel panel-default">
                                    <div class="panel-heading" role="tab" id="Two">
                                        <h4 class="panel-title">
                                            <a class="collapsed" role="button" data-toggle="collapse"
                                               data-parent="#accordion" href="#collapse_two" aria-expanded="false"
                                               aria-controls="collapse_two">Transferencia bancaria</a>
                                        </h4>
                                    </div>
                                    <div aria-expanded="false" id="collapse_two" class="panel-collapse collapse"
                                         role="tabpanel" aria-labelledby="Two">
                                        <div class="panel-body">
                                            <p>Realiza tus ingresos directamente en nuestra cuenta bancaria. Debes
                                                utilizar tu ID único de cliente como referencia en el pago.</p>
                                        </div>
                                    </div>
                                </div>
                                <!-- Single payment method -->
                                <div class="panel panel-default">
                                    <div class="panel-heading" role="tab" id="Three">
                                        <h4 class="panel-title">
                                            <a class="collapsed" role="button" data-toggle="collapse"
                                               data-parent="#accordion" href="#collapse_three" aria-expanded="false"
                                               aria-controls="collapse_three">Envio de cheque</a>
                                        </h4>
                                    </div>
                                    <div aria-expanded="false" id="collapse_three" class="panel-collapse collapse"
                                         role="tabpanel" aria-labelledby="Three">
                                        <div class="panel-body">
                                            <p>Envia un cheque con titular <b>miHost, Inc</b> a la siguiente dirección:
                                            </p>
                                            <p>Av. Alicante Num 22 - Gandia (46701)</p>
                                        </div>
                                    </div>
                                </div>
                                <!-- Single payment method -->
                                <div class="panel panel-default">
                                    <div class="panel-heading" role="tab" id="Four">
                                        <h4 class="panel-title">
                                            <a class="collapsed" role="button" data-toggle="collapse"
                                               data-parent="#accordion" href="#collapse_four" aria-expanded="false"
                                               aria-controls="collapse_four">PayPal</a>
                                        </h4>
                                    </div>
                                    <div aria-expanded="false" id="collapse_four" class="panel-collapse collapse"
                                         role="tabpanel" aria-labelledby="Four">
                                        <div class="panel-body">
                                            <p>Ingresa el email de tu cuenta en PayPal y paga a través de ellos.</p>
                                        </div>
                                        <div class="form-field form-field-paypal">
                                            <input name="paypal" placeholder="PayPal Email" type="text" size="15">
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- place order button -->
                            <div class="place_order_button">
                                <button type="submit" class="btn btn-default">Submit</button>
                            </div>
                            <div class="col-md-12">
                                <div class="form-field crate_account" align="right">
                                    <label>Acepto los términos</label>
                                    <input id="check_box" type="checkbox" required>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </form>
    </div>
</section>
<!-- ::::::::::::::::::: Checkout area end ::::::::::::::::::: -->

<!-- ::::::::::::::::::: Call to action Area Start ::::::::::::::::::: -->
<div class="call_to_action section_padding_60">
    <div class="container">
        <div class="row">
            <div class="col-xs-12">
                <!-- call to action text -->
                <div class="call_to wow fadeInUp" data-wow-delay="0.2s">
                    <h3>Selecciona el plan que mejor se adapte a tus necesidades!</h3>
                    <div class="call_to_action_button">
                        <a href="index.php#plans">Elige</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- ::::::::::::::::::: Call to action Area End ::::::::::::::::::: -->

<!-- ::::::::::::::::::: Partner Area Start ::::::::::::::::::: -->
<div class="partner_area">
    <div class="container">
        <div class="row">
            <div class="col-xs-12">
                <div class="partners_thumbs slide">
                    <!-- Single Partner Logo Area -->
                    <div class="single_partner_area">
                        <a href="">
                            <div class="single_partner_thumb">
                                <img src="img/partners-img/1.png" alt="">
                            </div>
                        </a>
                    </div>
                    <!-- Single Partner Logo Area -->
                    <div class="single_partner_area">
                        <a href="">
                            <div class="single_partner_thumb">
                                <img src="img/partners-img/2.png" alt="">
                            </div>
                        </a>
                    </div>
                    <!-- Single Partner Logo Area -->
                    <div class="single_partner_area">
                        <a href="">
                            <div class="single_partner_thumb">
                                <img src="img/partners-img/3.png" alt="">
                            </div>
                        </a>
                    </div>
                    <!-- Single Partner Logo Area -->
                    <div class="single_partner_area">
                        <a href="">
                            <div class="single_partner_thumb">
                                <img src="img/partners-img/4.png" alt="">
                            </div>
                        </a>
                    </div>
                    <!-- Single Partner Logo Area -->
                    <div class="single_partner_area">
                        <a href="">
                            <div class="single_partner_thumb">
                                <img src="img/partners-img/5.png" alt="">
                            </div>
                        </a>
                    </div>
                    <!-- Single Partner Logo Area -->
                    <div class="single_partner_area">
                        <a href="">
                            <div class="single_partner_thumb">
                                <img src="img/partners-img/6.png" alt="">
                            </div>
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- ::::::::::::::::::: Partner Area End ::::::::::::::::::: -->

<!-- ::::::::::::::::::: Footer Area Start ::::::::::::::::::: -->
<?php include("./php/include/footer.php"); ?>
<!-- ::::::::::::::::::: Footer Area End ::::::::::::::::::: -->

<!-- ::::::::::::::::::: All jQuery Plugins ::::::::::::::::::: -->

<!-- jQuery (necessary for all JavaScript plugins) -->
<script src="js/jquery-2.2.4.min.js"></script>

<!-- Bootstrap 3.3.7 js -->
<script src="js/bootstrap.min.js"></script>

<!-- Owl-carousel js -->
<script src="js/owl.carousel.min.js"></script>

<!-- Ajax Contact js -->
<script src="js/ajax-contact.js"></script>

<!-- Meanmenu js -->
<script src="js/meanmenu.js"></script>

<!-- Waypoint js -->
<script src="js/waypoints.min.js"></script>

<!-- Counterup js -->
<script src="js/counterup.min.js"></script>

<!-- jQuery easing js -->
<script src="js/jquery.easing.1.3.js"></script>

<!-- ScrollUp js -->
<script src="js/jquery.scrollUp.js"></script>

<!-- WoW js -->
<script src="js/wow.min.js"></script>

<!-- Active js -->
<script src="js/active.js"></script>

<!-- skeuocard includes (card number checker) -->
<script src="./src/skeuocard/javascripts/vendor/demo.fix.js"></script>
<script src="./src/skeuocard/javascripts/skeuocard.js"></script>
<script>
    new Skeuocard($("#skeuocard"), {
        initialValues: {
            number: "4"
        }
    });

</script>
</body>

</html>
<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

