<!DOCTYPE html>
<html lang="es">

<!-- Head tag -->
<head>
    <meta charset="UTF-8">
    <meta name="description" content="">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->

    <!-- Title  -->
    <title>miHost - Cart</title>

    <!-- Favicon  -->
    <link rel="shortcut icon" href="img/core-img/favicon.ico">

    <!-- ::::::::::::::::::: All CSS Files ::::::::::::::::::: -->

    <!-- Style css -->
    <link rel="stylesheet" href="style.css">

    <!-- Responsive css -->
    <link rel="stylesheet" href="css/responsive.css">

    <!--[if IE]>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/html5shiv/3.7.3/html5shiv.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->

</head>

<!-- Body tag -->
<body>
<!-- ::::::::::::::::::: include login.php ::::::::::::::::::: -->
<?php include("./php/include/login.php"); ?>
<!-- ::::::::::::::::::: Header End ::::::::::::::::::: -->

<!-- ::::::::::::::::::: include header.php ::::::::::::::::::: -->
<?php include("./php/include/header.php"); ?>
<!-- ::::::::::::::::::: Header End ::::::::::::::::::: -->

<!-- ::::::::::::::::::: Breadcumb area start ::::::::::::::::::: -->
<section class="breadcumb_area">
    <div class="container">
        <div class="row">
            <div class="col-xs-12">
                <div class="breadcumb_section">
                    <!-- Breadcumb page title start -->
                    <div class="page_title">
                        <h3>Tu carrito</h3>
                    </div>
                    <!-- Breadcumb start -->
                    <ol class="breadcrumb">
                        <li><a href="index.php">Home</a></li>
                        <li class="active">Carrito</li>
                    </ol>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- ::::::::::::::::::: Breadcumb area End ::::::::::::::::::: -->

<!-- ::::::::::::::::::: Shopping cart area start ::::::::::::::::::: -->
<section class="shopping_cart section_padding_100">
    <div class="container">
        <?php
        if (!isset($_SESSION['cart']['coupon']['discount'])) {
            $_SESSION['cart']['coupon']['discount'] = 0;
        }
        if (isset($_SESSION['cart']['domain']) && isset($_SESSION['cart']['plan'])){
        switch ($_SESSION['cart']['plan']) {
            case "basic":
                $price['plan'] = 4.99;
                break;
            case "standard":
                $price['plan'] = 12.99;
                break;
            case "business":
                $price['plan'] = 24.99;
                break;
            case "unlimited":
                $price['plan'] = 59.99;
                break;
        }

        preg_match('/([a-z]+)(\.[a-z]+)$/', $_SESSION['cart']['domain'], $clean_domain);
        //var_dump($clean_domain);
        $domain_search = $clean_domain[0];
        $domain_name = $clean_domain[1];
        $domain_tld = $clean_domain[2];
        switch ($domain_tld) {
            case ".ml":
            case ".tk":
            case ".ga":
            case ".gq":
            case ".cf":
                $price['domain'] = 0;
                break;
            case ".es":
            case ".com":
                $price['domain'] = 10;
                break;
            case ".net":
            case ".org":
                $price['domain'] = 15;
                break;
            default:
                $price['domain'] = 30;
                break;
        }
        ?>
        <div class="row">
            <div class="col-xs-12">
                <div class="cart_table table-responsive">
                    <table class="cart">
                        <!--  Table Head  -->
                        <thead>
                        <tr>
                            <th class="p_name">Producto</th>
                            <th class="p_duration">Duracion</th>
                            <th class="p_price">Precio</th>
                        </tr>
                        </thead>
                        <!--  Table Body  -->
                        <tbody>
                        <!-- Single Item  -->
                        <tr class="cart_single_item">
                            <td class="p_name">
                                <i class="fa fa-home" aria-hidden="true"></i>
                                <a href="#">Dominio: <?php echo $_SESSION['cart']['domain']; ?></a>
                            </td>
                            <td class="p_duration">
                                <span>1 Año</span>
                            </td>
                            <td class="p_price">
                                <span class="amount"><?php echo $price['domain']; ?>€</span>
                            </td>
                        </tr>
                        <!-- Single Item  -->
                        <tr class="cart_single_item">
                            <td class="p_name">
                                <i class="fa fa-paperclip" aria-hidden="true"></i>
                                <a href="#">Plan: <?php echo $_SESSION['cart']['plan']; ?></a>
                            </td>
                            <td class="p_duration">
                                <span>1 Mes</span>
                            </td>
                            <td class="p_price">
                                <span class="amount"><?php echo $price['plan']; ?>€</span>
                            </td>
                        </tr>
                        </tbody>
                    </table>
                </div>
                <form class="cart_form" action="#" method="post">
                    <div class="row">
                        <div class="col-xs-12 col-sm-6">
                            <form action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]) . '#coupon'; ?>">
                                <!-- Continue Shop Button  -->
                                <div class="continue_shop_button">
                                    <a class="btn btn-default continue_button" role="button"
                                       href="./php/session.php?destroy">Vaciar carrito</a>
                                </div>
                                <!-- Coupon  -->
                                <div id="coupon" class="coupon">
                                    <?php
                                    if ($_SESSION['cart']['coupon']['status'] == TRUE) {
                                        ?>
                                        <h3>Cupón
                                            aplicado: <?php echo $_SESSION['cart']['coupon']['discount'] . '%' ?></h3>
                                        <p>¿Quieres cambiarlo?...</p>
                                        <input type="text" name="coupon_code" class="coupon_code"
                                               placeholder="••••••••">
                                        <input type="submit" class="button" name="app_coupon" value="Aplicar">
                                        <?php
                                    } else {
                                        ?>
                                        <h3>¿Tienes algún cupón?</h3>
                                        <p>Inserta el codigo aqui...</p>
                                        <input type="text" name="coupon_code" class="coupon_code"
                                               placeholder="••••••••">
                                        <input type="submit" class="button" name="app_coupon" value="Aplicar">
                                        <?php
                                    }
                                    ?>
                                </div>
                            </form>
                            <?php
                            if ($_SERVER['REQUEST_METHOD'] == 'POST') {
                                // prevent XSS
                                $_POST = filter_input_array(INPUT_POST, FILTER_SANITIZE_STRING);
                                if (isset ($_POST['coupon_code'])) {
                                    if (preg_match('/^(w){1}([1,3,5,7,9]){1}([a,e,i,o,u]){1}(\/){1}(PROMO){1}$/', $_POST['coupon_code'])) {
                                        echo "Aplicado";
                                        $_SESSION['cart']['coupon']['status'] = TRUE;
                                        $_SESSION['cart']['coupon']['discount'] = 5;
                                    } else {
                                        echo "Invalido";
                                    }
                                }
                            }
                            ?>
                        </div>
                        <!-- Cart Total -->
                        <div class="col-xs-12 col-sm-6">
                            <div class="cart_total">
                                <h2>Pago a realizar</h2>
                                <table>
                                    <tbody>
                                    <tr class="c_subtotal">
                                        <th>Subtotal</th>
                                        <td><span class="amount"><?php echo $price['plan'] + $price['domain']; ?>
                                                €</span></td>
                                    </tr>
                                    <tr class="discount">
                                        <th>Descuento: <?php echo $_SESSION['cart']['coupon']['discount'] ?>%</th>
                                        <td>
                                            <span class="amount"><?php echo (($price['plan'] + $price['domain']) * $_SESSION['cart']['coupon']['discount']) / 100; ?>
                                                €</span></td>
                                    </tr>
                                    <tr class="order_total">
                                        <th>Total</th>
                                        <td>
                                            <span class="amount"><?php echo ($price['plan'] + $price['domain']) - ((($price['plan'] + $price['domain']) * $_SESSION['cart']['coupon']['discount']) / 100); ?>
                                                €</span></td>
                                    </tr>
                                    </tbody>
                                </table>
                                <!-- Checkout Button  -->
                                <div class="checkout_button">
                                    <a href="checkout.php" class="btn btn-default che_button" role="button">Realizar
                                        pago</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>

                <?php
                }
                if (!isset($_SESSION['cart']['domain'])) {
                    ?>
                    <div class="row">
                        <div class="col-xs-12">
                            <div class="success_failure_result m_top_30">
                                <div class="notification fail">
                                    <h3>Todavia <span>no</span> has elegido un dominio!</h3>
                                </div>
                                <div class="add_to_cart_btn">
                                    <a class="btn btn-default" href="index.php#domain" role="button">Elige tu
                                        dominio</a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <?php
                }
                if (!isset($_SESSION['cart']['plan'])) {
                    ?>
                    <div class="row">
                        <div class="col-xs-12">
                            <div class="success_failure_result m_top_30">
                                <div class="notification fail">
                                    <h3>Todavia <span>no</span> has elegido un plan!</h3>
                                </div>
                                <div class="add_to_cart_btn">
                                    <a class="btn btn-default" href="index.php#plans" role="button">Elige tu plan</a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <?php
                }
                if (isset($_SESSION['cart']['domain']) && !isset($_SESSION['cart']['plan'])) {
                    ?>
                    <div class="row">
                        <div class="col-xs-12">
                            <div class="success_failure_result m_top_30">
                                <div class="notification cart">
                                    <h3>El dominio <span><?php echo $_SESSION['cart']['domain'] ?></span> ha sido
                                        incluido en tu carrito.</h3>
                                </div>
                                <div class="add_to_cart_btn">
                                    <a class="btn btn-default" href="index.php#domain" role="button">Cambiar dominio</a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <?php
                }
                if (isset($_SESSION['cart']['plan']) && !isset($_SESSION['cart']['domain'])) {
                    ?>
                    <div class="row">
                        <div class="col-xs-12">
                            <div class="success_failure_result m_top_30">
                                <div class="notification cart">
                                    <h3>El plan <span><?php echo $_SESSION['cart']['plan'] ?></span> ha sido incluido en
                                        tu carrito.</h3>
                                </div>
                                <div class="add_to_cart_btn">
                                    <a class="btn btn-default" href="index.php#plans" role="button">Cambiar de plan</a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <?php
                }
                ?>
                <!-- Cart Form  -->
            </div>
        </div>
    </div>
</section>
<!-- ::::::::::::::::::: Shopping cart area end ::::::::::::::::::: -->

<!-- ::::::::::::::::::: Call to action Area Start ::::::::::::::::::: -->
<div class="call_to_action section_padding_60">
    <div class="container">
        <div class="row">
            <div class="col-xs-12">
                <!-- call to action text -->
                <div class="call_to wow fadeInUp" data-wow-delay="0.2s">
                    <h3>Selecciona el plan que mejor se adapte a tus necesidades!</h3>
                    <div class="call_to_action_button">
                        <a href="index.php#plans">Elige</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- ::::::::::::::::::: Call to action Area End ::::::::::::::::::: -->

<!-- ::::::::::::::::::: Partner Area Start ::::::::::::::::::: -->
<div class="partner_area">
    <div class="container">
        <div class="row">
            <div class="col-xs-12">
                <div class="partners_thumbs slide">
                    <!-- Single Partner Logo Area -->
                    <div class="single_partner_area">
                        <a href="">
                            <div class="single_partner_thumb">
                                <img src="img/partners-img/1.png" alt="">
                            </div>
                        </a>
                    </div>
                    <!-- Single Partner Logo Area -->
                    <div class="single_partner_area">
                        <a href="">
                            <div class="single_partner_thumb">
                                <img src="img/partners-img/2.png" alt="">
                            </div>
                        </a>
                    </div>
                    <!-- Single Partner Logo Area -->
                    <div class="single_partner_area">
                        <a href="">
                            <div class="single_partner_thumb">
                                <img src="img/partners-img/3.png" alt="">
                            </div>
                        </a>
                    </div>
                    <!-- Single Partner Logo Area -->
                    <div class="single_partner_area">
                        <a href="">
                            <div class="single_partner_thumb">
                                <img src="img/partners-img/4.png" alt="">
                            </div>
                        </a>
                    </div>
                    <!-- Single Partner Logo Area -->
                    <div class="single_partner_area">
                        <a href="">
                            <div class="single_partner_thumb">
                                <img src="img/partners-img/5.png" alt="">
                            </div>
                        </a>
                    </div>
                    <!-- Single Partner Logo Area -->
                    <div class="single_partner_area">
                        <a href="">
                            <div class="single_partner_thumb">
                                <img src="img/partners-img/6.png" alt="">
                            </div>
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- ::::::::::::::::::: Partner Area End ::::::::::::::::::: -->

<!-- ::::::::::::::::::: Footer Area Start ::::::::::::::::::: -->
<?php include("./php/include/footer.php"); ?>
<!-- ::::::::::::::::::: Footer Area End ::::::::::::::::::: -->

<!-- ::::::::::::::::::: All jQuery Plugins ::::::::::::::::::: -->

<!-- jQuery (necessary for all JavaScript plugins) -->
<script src="js/jquery-2.2.4.min.js"></script>

<!-- Bootstrap 3.3.7 js -->
<script src="js/bootstrap.min.js"></script>

<!-- Owl-carousel js -->
<script src="js/owl.carousel.min.js"></script>

<!-- Ajax Contact js -->
<script src="js/ajax-contact.js"></script>

<!-- Meanmenu js -->
<script src="js/meanmenu.js"></script>

<!-- Waypoint js -->
<script src="js/waypoints.min.js"></script>

<!-- Counterup js -->
<script src="js/counterup.min.js"></script>

<!-- jQuery easing js -->
<script src="js/jquery.easing.1.3.js"></script>

<!-- ScrollUp js -->
<script src="js/jquery.scrollUp.js"></script>

<!-- WoW js -->
<script src="js/wow.min.js"></script>

<!-- Active js -->
<script src="js/active.js"></script>

</body>

</html>


<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

