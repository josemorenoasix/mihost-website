<!DOCTYPE html>
<html lang="es">

<!-- Head tag -->
<head>
    <meta charset="UTF-8">
    <meta name="description" content="">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->

    <!-- Title  -->
    <title>miHost - Home</title>

    <!-- Favicon  -->
    <link rel="shortcut icon" href="img/core-img/favicon.ico">

    <!-- ::::::::::::::::::: All CSS Files ::::::::::::::::::: -->

    <!-- Style css -->
    <link rel="stylesheet" href="style.css">

    <!-- Responsive css -->
    <link rel="stylesheet" href="css/responsive.css">

    <!--[if IE]>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/html5shiv/3.7.3/html5shiv.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->

</head>

<!-- Body tag -->
<body>
<!-- ::::::::::::::::::: include login.php ::::::::::::::::::: -->
<?php include("./php/include/login.php"); ?>
<!-- ::::::::::::::::::: Header End ::::::::::::::::::: -->

<!-- ::::::::::::::::::: include header.php ::::::::::::::::::: -->
<?php include("./php/include/header.php"); ?>
<!-- ::::::::::::::::::: Header End ::::::::::::::::::: -->

<!-- ::::::::::::::::::::::::::: Welcome Area Start ::::::::::::::::::::::::::: -->
<section class="welcome_area">
    <div class="container-fluid">
        <div class="row">
            <div class="col-xs-12">
                <div class="welcome_slider">
                    <!-- Single Slide Start -->
                    <div class="single_slide" style="background-image: url(img/bg-pattern/welcome-bg.jpg);">
                        <div class="slide_text">
                            <div class="table">
                                <div class="table_cell">
                                    <h2><span>Bienvenido al </span>mundo online</h2>
                                    <h3>Proveemos servicios hosting</h3>
                                    <div class="welcome_btn">
                                        <a class="btn btn-1 btn-default btn-lg" href="#domain" role="button">Empezar
                                            ahora!</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <!-- Single Slide Start -->
                    <div class="single_slide" style="background-image: url(img/bg-pattern/welcome-bg.jpg);">
                        <div class="slide_text">
                            <div class="table">
                                <div class="table_cell">
                                    <h2>Amplia tu negocio <span>1-Click CMS</span></h2>
                                    <h3> Create una tienda prestashop y un blog en wordpress con un par de clicks</h3>
                                    <div class="welcome_btn">
                                        <a class="btn btn-1 btn-default btn-lg" href="#domain" role="button">Empezar
                                            ahora!</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Cloud Area Start -->
    <div class="clouds">
        <img src="img/core-img/cloud-1.png" alt="" class="cloud-1">
        <img src="img/core-img/cloud-2.png" alt="" class="cloud-2">
        <img src="img/core-img/cloud-3.png" alt="" class="cloud-3">
        <img src="img/core-img/cloud-4.png" alt="" class="cloud-4">
        <img src="img/core-img/cloud-5.png" alt="" class="cloud-5">
    </div>
</section>
<!-- ::::::::::::::::::::::::::: Welcome Area End ::::::::::::::::::::::::::: -->
<?php
if (isset($_SESSION['cart']['domain']) || isset($_SESSION['cart']['plan'])){
?>
<section id="summary">
    <div class="price_plan_area section_padding_100_70" data-wow-delay="0.2s">
        <div class="container">
            <div class="row">
                <div class="col-xs-12">
                    <div class="section_heading wow fadeInUp">
                        <p>Aqui tienes un</p>
                        <h3>Resumen <span>De Tu Compra</span></h3>
                    </div>
                    <div class="success_failure_result cool_fact_text wow fadeInUp" data-wow-delay="0.4s">
                        <?php
                        if (isset($_SESSION['cart']['domain'])) {
                            ?>
                            <div class="notification">
                                <h3>El dominio <span><?php echo $_SESSION['cart']['domain'] ?></span> está en tu
                                    carrito.</h3>
                            </div>
                            <div class="add_to_cart_btn">
                                <a class="btn btn-default" href="#domain" role="button">Elige otro</a>
                            </div>
                        <?php } else { ?>
                            <div class="notification fail">
                                <h3>Todavia no has ningun tu dominio.</h3>
                            </div>
                            <div class="add_to_cart_btn">
                                <a class="btn btn-default" href="#domain" role="button">Eligelo</a>
                            </div>
                        <?php } ?>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-xs-12">
                    <div class="success_failure_result cool_fact_text wow fadeInUp" data-wow-delay="0.6s">
                        <?php
                        if (isset($_SESSION['cart']['plan'])) {
                            ?>
                            <div class="notification">
                                <h3>El plan <span><?php echo $_SESSION['cart']['plan'] ?></span> está en tu carrito.
                                </h3>
                            </div>
                            <div class="add_to_cart_btn">
                                <a class="btn btn-default" href="#plans" role="button">Elige otro</a>
                            </div>
                        <?php } else { ?>
                            <div class="notification fail">
                                <h3>Todavia no has elegido ningún plan.</h3>
                            </div>
                            <div class="add_to_cart_btn">
                                <a class="btn btn-default" href="#plans" role="button">Eligelo</a>
                            </div>
                        <?php } ?>
                    </div>
                </div>
            </div>
            <?php
            }
            if (isset($_SESSION['cart']['domain']) && isset($_SESSION['cart']['plan'])) {
                ?>
                <div class="row">
                    <div class="col-xs-12">
                        <div class="success_failure_result_summary cool_fact_text wow fadeInUp" data-wow-delay="0.8s">
                            <div class="get_started_button">
                                <a class="btn btn-default btn-block" href="cart.php" role="button">Revisar el
                                    carrito</a>
                            </div>
                        </div>
                    </div>
                </div>
                <?php
            }
            ?>
        </div>
    </div>
</section>
<!-- ::::::::::::::::::: Domain Search Box Area Start ::::::::::::::::::: -->
<section id="domain" class="domain_search_area section_padding_60">
    <div class="container">
        <div class="row">
            <div class="col-md-10 col-md-offset-1 cool_fact_text wow fadeInUp" data-wow-delay="0.4s">
                <!-- Search Title -->
                <div class="search_text">
                    <h4>Encuentra el dominio que buscas...</h4>
                </div>
                <!-- Search Form Start -->
                <form method="post" action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]) . '#domain'; ?>"
                      class="domain_form">
                    <div class="form-group">
                        <input type="text" name="searchdomain" class="form-control search_box" placeholder=". . ."
                               required>
                    </div>
                    <button type="submit" class="btn btn-default submit_btn">Buscar</button>
                </form>
                <?php
                session_start();
                if ($_SERVER['REQUEST_METHOD'] === 'POST') {
                    // prevent XSS
                    $_POST = filter_input_array(INPUT_POST, FILTER_SANITIZE_STRING);
                    // View Regex https://www.debuggex.com/r/y4Xe_hDVO11bv1DV
                    $regex = '/^(([a-zA-Z]{1})|([a-zA-Z]{1}[a-zA-Z]{1})|([a-zA-Z]{1}[0-9]{1})|([0-9]{1}[a-zA-Z]{1})|([a-zA-Z0-9][a-zA-Z0-9-_]{1,61}[a-zA-Z0-9]))\.([a-zA-Z]{2,6}|[a-zA-Z0-9-]{2,30}\.[a-zA-Z]{2,3})$/';
                    if (isset ($_POST['searchdomain'])) {
                        preg_match($regex, $_POST['searchdomain'], $clean_domain);
                        $domain_search = $clean_domain[0];
                        require_once './src/Unirest.php';
                        $json_response = Unirest\Request::get("https://domainr.p.mashape.com/v2/status?mashape-key=&domain=$domain_search",
                            array(
                                "X-Mashape-Key" => "xRhYeXxuJbmsh6eU3dTr8MwsMbbrp1eKcKqjsn1J8BTtBoYjBj",
                                "Accept" => "application/json"
                            )
                        );
                        $response = json_decode(json_encode($json_response), true);
                        if ($response['body']['status'][0]['summary'] == 'inactive') {
                            ?>
                            <form action="./php/session.php?newdomain" method="post">
                                <div class="success_failure_result m_top_30">
                                    <div class="notification">
                                        <h3>Enhorabuena!
                                            <span><?php echo $response['body']['status'][0]['domain'] ?></span> está
                                            disponible.</h3>
                                    </div>
                                    <div class="add_to_cart_btn">
                                        <input type="hidden" name="cartdomain"
                                               value="<?php echo $response['body']['status'][0]['domain'] ?>">
                                        <a class="btn btn-default" href="#" role="button" type="submit"
                                           onclick="$(this).closest('form').submit()">Al carrito!</a>
                                    </div>
                                </div>
                            </form>
                            <?php
                        } // end if summary=='inactive'
                        else { ?>
                            <div class="success_failure_result m_top_30">
                                <div class="notification fail">
                                    <h3>Que pena! <span><?php echo $response['body']['status'][0]['domain'] ?></span> no
                                        está disponible.</h3>
                                </div>
                            </div>
                            <?php
                        } // end else summary=='inactive'
                    } // end if POST['searchdomain']
                } // end if REQUEST_METHOD
                ?>

            </div>
        </div>
    </div>
</section>
<!-- ::::::::::::::::::: Domain Search Box Area End ::::::::::::::::::: -->

<!-- ::::::::::::::::::: Price and Plans Area Start ::::::::::::::::::: -->
<div id="plans" class="price_plan_area section_padding_100_70">
    <div class="container">
        <div class="row">
            <div class="col-xs-12">
                <!-- Section Heading Start -->

                <div class="section_heading wow fadeInUp">
                    <p>Ahora elige uno de</p>
                    <h3>Nuestros <span>Planes</span></h3>
                </div>
                <!-- Section Heading End -->
            </div>
        </div>
    </div>
    <div class="container">
        <div class="row">
            <!-- Single Price Plan Area Start -->
            <form action="./php/session.php?newplan" method="post">
                <div class="col-sm-6 col-md-3">
                    <div class="single_price_plan wow fadeInUp <?php if ($_SESSION['cart']['plan'] == 'basic') {
                        echo "selected";
                    } ?>" data-wow-delay="0.2s">
                        <div class="title">
                            <h3>Basic</h3>
                        </div>
                        <div class="price">
                            <h4>4,99 €/mes</h4>
                        </div>
                        <div class="description">
                            <p>Espacio web: 2GiB</p>
                            <p>Encriptado SSL/TLS</p>
                            <p>Usuarios FTP: 1</p>
                            <p>Buzones de correo<br> 2 x 512MiB</p>
                            <p>Bases de datos<br> 1 x 512MiB</p>
                        </div>
                        <div class="button">
                            <input type="hidden" name="cartplan" value="basic">
                            <a class="btn btn-default" href="#" role="button" type="submit"
                               onclick="$(this).closest('form').submit()">Al carrito!</a>
                        </div>
                    </div>
                </div>
            </form>
            <!-- Single Price Plan Area End -->

            <!-- Single Price Plan Area Start -->
            <form action="./php/session.php?newplan" method="post">
                <div class="col-sm-6 col-md-3">
                    <div class="single_price_plan wow fadeInUp <?php if ($_SESSION['cart']['plan'] == 'standard') {
                        echo "selected";
                    } ?>" data-wow-delay="0.4s">
                        <div class="title">
                            <h3>Standard</h3>
                        </div>
                        <div class="price">
                            <h4>12.99 €/mes</h4>
                        </div>
                        <div class="description">
                            <p>Espacio web: 10GiB</p>
                            <p>Encriptado SSL/TLS</p>
                            <p>Usuarios FTP: 5</p>
                            <p>Buzones de correo<br> 5 x 512MiB</p>
                            <p>Bases de datos<br> 2 x 1GiB</p>
                        </div>
                        <div class="button">
                            <input type="hidden" name="cartplan" value="standard">
                            <a class="btn btn-default" href="#" role="button" type="submit"
                               onclick="$(this).closest('form').submit()">Al carrito!</a>
                        </div>
                    </div>
                </div>
            </form>
            <!-- Single Price Plan Area End -->

            <!-- Single Price Plan Area Start -->
            <form action="./php/session.php?newplan" method="post">
                <div class="col-sm-6 col-md-3">
                    <div class="single_price_plan wow fadeInUp <?php if ($_SESSION['cart']['plan'] == 'business') {
                        echo "selected";
                    } ?>" data-wow-delay="0.6s">
                        <div class="title">
                            <h3>Business</h3>
                        </div>
                        <div class="price">
                            <h4>24,99 €/mes</h4>
                        </div>
                        <div class="description">
                            <p>Espacio web: 30GiB</p>
                            <p>Encriptado SSL/TLS</p>
                            <p>Usuarios FTP: 20</p>
                            <p>Buzones de correo<br> 15 x 512MiB</p>
                            <p>Bases de datos<br> 5 x 2GiB</p>
                        </div>
                        <div class="button">
                            <input type="hidden" name="cartplan" value="business">
                            <a class="btn btn-default" href="#" role="button" type="submit"
                               onclick="$(this).closest('form').submit()">Al carrito!</a>
                        </div>
                    </div>
                </div>
            </form>

            <!-- Single Price Plan Area End -->

            <!-- Single Price Plan Area Start -->
            <form action="./php/session.php?newplan" method="post">
                <div class="col-sm-6 col-md-3">
                    <div class="single_price_plan wow fadeInUp <?php if ($_SESSION['cart']['plan'] == 'unlimited') {
                        echo "selected";
                    } ?>" data-wow-delay="0.8s">
                        <div class="title">
                            <h3>Unlimited</h3>
                        </div>
                        <div class="price">
                            <h4>59,99 €/mes</h4>
                        </div>
                        <div class="description">
                            <p>Espacio web: <b>100GiB</b></p>
                            <p>Encriptado SSL/TLS</p>
                            <p>Usuarios FTP: 100</p>
                            <p>Buzones de correo<br> 40 x 1GiB</p>
                            <p>Bases de datos<br> 8 x 10GiB</p>
                        </div>
                        <div class="button">
                            <input type="hidden" name="cartplan" value="unlimited">
                            <a class="btn btn-default" href="#" role="button" type="submit"
                               onclick="$(this).closest('form').submit()">Al carrito!</a>
                        </div>
                    </div>
                </div>
            </form>
            <!-- Single Price Plan Area End -->
        </div>
    </div>
</div>
</form>

<!-- ::::::::::::::::::: Call to action Area Start ::::::::::::::::::: -->
<div class="call_to_action section_padding_60">
    <div class="container">
        <div class="row">
            <div class="col-xs-12">
                <!-- call to action text -->
                <div class="call_to wow fadeInUp" data-wow-delay="0.2s">
                    <h3>Reserva tu propio dominio!</h3>
                    <div class="call_to_action_button">
                        <a href="#domain">Comprobar</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- ::::::::::::::::::: Call to action Area End ::::::::::::::::::: -->

<!-- ::::::::::::::::::: Our Services Area Start ::::::::::::::::::: -->
<section class="services_area section_padding_100_70">
    <div class="container">
        <div class="row">
            <div class="col-xs-12">
                <!-- Section Heading Start -->
                <div class="section_heading wow fadeInUp">
                    <p> ¿Que podemos ofrecerte?</p>
                    <h3>Nuestros <span>Servicios</span></h3>
                </div>
                <!-- Section Heading End -->
            </div>
        </div>

        <div class="row">

            <!-- Single Service Area Start -->
            <div class="col-sm-6 col-md-4">
                <div class="single_service wow fadeInUp" data-wow-delay="0.2s">
                    <!-- Single Service icon -->
                    <div class="single_service_icon">
                        <span class="icon-global"></span>
                    </div>
                    <!-- Single Service title -->
                    <div class="single_service_text">
                        <h4>Alojamiento web</h4>
                        <p>Tenemos varios paquetes que incluyen el servicio web. Elige el plan que mejor se adapte.</p>
                    </div>
                </div>
            </div>

            <!-- Single Service Area Start -->
            <div class="col-sm-6 col-md-4">
                <div class="single_service wow fadeInUp" data-wow-delay="0.4s">
                    <!-- Single Service icon -->
                    <div class="single_service_icon">
                        <span class="icon-basket"></span>
                    </div>
                    <!-- Single Service title -->
                    <div class="single_service_text">
                        <h4>Cuentas de correo</h4>
                        <p>Accede a tus cuentas con tu aplicacion de escritorio preferida o a través de webmail.</p>
                    </div>
                </div>
            </div>

            <!-- Single Service Area Start -->
            <div class="col-sm-6 col-md-4">
                <div class="single_service wow fadeInUp" data-wow-delay="0.6s">
                    <!-- Single Service icon -->
                    <div class="single_service_icon">
                        <span class="icon-gears"></span>
                    </div>
                    <!-- Single Service title -->
                    <div class="single_service_text">
                        <h4>Acceso a cPanel</h4>
                        <p>Adminsitra tu dominio de forma sencilla desde el panel de administracion.</p>
                    </div>
                </div>
            </div>

            <!-- Single Service Area Start -->
            <div class="col-sm-6 col-md-4">
                <div class="single_service wow fadeInUp" data-wow-delay="0.8s">
                    <!-- Single Service icon -->
                    <div class="single_service_icon">
                        <span class="icon-upload"></span>
                    </div>
                    <!-- Single Service title -->
                    <div class="single_service_text">
                        <h4>Alta Disponibilidad</h4>
                        <p>Tu negocio siempre en linea. Disponibilidad del 99.9%.</p>
                    </div>
                </div>
            </div>

            <!-- Single Service Area Start -->
            <div class="col-sm-6 col-md-4">
                <div class="single_service wow fadeInUp" data-wow-delay="1s">
                    <!-- Single Service icon -->
                    <div class="single_service_icon">
                        <span class="icon-chat"></span>
                    </div>
                    <!-- Single Service title -->
                    <div class="single_service_text">
                        <h4>Soporte 24/7</h4>
                        <p>Utiliza el formulario de contacto y contacta con nosotros en cualquier momento.</p>
                    </div>
                </div>
            </div>

            <!-- Single Service Area Start -->
            <div class="col-sm-6 col-md-4">
                <div class="single_service wow fadeInUp" data-wow-delay="1.2s">
                    <!-- Single Service icon -->
                    <div class="single_service_icon">
                        <span class="icon-laptop"></span>
                    </div>
                    <!-- Single Service title -->
                    <div class="single_service_text">
                        <h4>1-Click CMS</h4>
                        <p>Crea tu blog o tienda virtual con wordpress, prestashop, moodle...</p>
                    </div>
                </div>
            </div>

        </div>
        <!-- end. row -->
    </div>
    <!-- end. container -->
</section>
<!-- ::::::::::::::::::: Our Services Area End ::::::::::::::::::: -->

<!-- ::::::::::::::::::: Call to action Area Start ::::::::::::::::::: -->
<div class="call_to_action section_padding_60">
    <div class="container">
        <div class="row">
            <div class="col-xs-12">
                <!-- call to action text -->
                <div class="call_to wow fadeInUp" data-wow-delay="0.2s">
                    <h3>Selecciona el plan que mejor se adapte a tus necesidades!</h3>
                    <div class="call_to_action_button">
                        <a href="#plans">Elige</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- ::::::::::::::::::: Call to action Area End ::::::::::::::::::: -->

<!-- ::::::::::::::::::: Partner Area Start ::::::::::::::::::: -->
<div class="partner_area">
    <div class="container">
        <div class="row">
            <div class="col-xs-12">
                <div class="partners_thumbs slide">
                    <!-- Single Partner Logo Area -->
                    <div class="single_partner_area">
                        <a href="">
                            <div class="single_partner_thumb">
                                <img src="img/partners-img/1.png" alt="">
                            </div>
                        </a>
                    </div>
                    <!-- Single Partner Logo Area -->
                    <div class="single_partner_area">
                        <a href="">
                            <div class="single_partner_thumb">
                                <img src="img/partners-img/2.png" alt="">
                            </div>
                        </a>
                    </div>
                    <!-- Single Partner Logo Area -->
                    <div class="single_partner_area">
                        <a href="">
                            <div class="single_partner_thumb">
                                <img src="img/partners-img/3.png" alt="">
                            </div>
                        </a>
                    </div>
                    <!-- Single Partner Logo Area -->
                    <div class="single_partner_area">
                        <a href="">
                            <div class="single_partner_thumb">
                                <img src="img/partners-img/4.png" alt="">
                            </div>
                        </a>
                    </div>
                    <!-- Single Partner Logo Area -->
                    <div class="single_partner_area">
                        <a href="">
                            <div class="single_partner_thumb">
                                <img src="img/partners-img/5.png" alt="">
                            </div>
                        </a>
                    </div>
                    <!-- Single Partner Logo Area -->
                    <div class="single_partner_area">
                        <a href="">
                            <div class="single_partner_thumb">
                                <img src="img/partners-img/6.png" alt="">
                            </div>
                        </a>
                    </div>
                    <!-- Single Partner Logo Area -->
                    <div class="single_partner_area">
                        <a href="">
                            <div class="single_partner_thumb">
                                <img src="img/partners-img/7.png" alt="">
                            </div>
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- ::::::::::::::::::: Partner Area End ::::::::::::::::::: -->

<!-- ::::::::::::::::::: Footer Area Start ::::::::::::::::::: -->
<?php include("./php/include/footer.php"); ?>
<!-- ::::::::::::::::::: Footer Area End ::::::::::::::::::: -->

<!-- ::::::::::::::::::: All jQuery Plugins ::::::::::::::::::: -->

<!-- jQuery (necessary for all JavaScript plugins) -->
<script src="js/jquery-2.2.4.min.js"></script>

<!-- Bootstrap 3.3.7 js -->
<script src="js/bootstrap.min.js"></script>

<!-- Owl-carousel js -->
<script src="js/owl.carousel.min.js"></script>

<!-- Ajax Contact js -->
<script src="js/ajax-contact.js"></script>

<!-- Meanmenu js -->
<script src="js/meanmenu.js"></script>

<!-- Waypoint js -->
<script src="js/waypoints.min.js"></script>

<!-- Counterup js -->
<script src="js/counterup.min.js"></script>

<!-- jQuery easing js -->
<script src="js/jquery.easing.1.3.js"></script>

<!-- ScrollUp js -->
<script src="js/jquery.scrollUp.js"></script>

<!-- WoW js -->
<script src="js/wow.min.js"></script>

<!-- Active js -->
<script src="js/active.js"></script>

</body>

</html>


