<!DOCTYPE html>
<html lang="es">

<!-- Head tag -->
<head>
    <meta charset="UTF-8">
    <meta name="description" content="">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->

    <!-- Title  -->
    <title>miHost - Signup</title>

    <!-- Favicon  -->
    <link rel="shortcut icon" href="img/core-img/favicon.ico">

    <!-- ::::::::::::::::::: All CSS Files ::::::::::::::::::: -->

    <!-- Style css -->
    <link rel="stylesheet" href="style.css">

    <!-- Responsive css -->
    <link rel="stylesheet" href="css/responsive.css">

    <!-- Font-awesome css -->
    <link rel="stylesheet" href="css/font-awesome.min.css">

    <!--[if IE]>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/html5shiv/3.7.3/html5shiv.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->


</head>
<!-- Body tag -->
<body>
<!-- ::::::::::::::::::: include login.php ::::::::::::::::::: -->
<?php include("./php/include/login.php"); ?>
<!-- ::::::::::::::::::: Header End ::::::::::::::::::: -->

<!-- ::::::::::::::::::: include header.php ::::::::::::::::::: -->
<?php include("./php/include/header.php"); ?>
<!-- ::::::::::::::::::: Header End ::::::::::::::::::: -->


<?php
session_start();
if (!isset($_SESSION['cart']['plan']) || !isset($_SESSION['cart']['domain']) || !isset($_POST)) {
    header("Locate: ./index.php");
}
if ($_SERVER['REQUEST_METHOD'] === 'POST') {
    include("./php/include/plans.php");
}
?>
<!-- ::::::::::::::::::: Breadcumb area start ::::::::::::::::::: -->
<section class="breadcumb_area">
    <div class="container">
        <div class="row">
            <div class="col-xs-12">
                <div class="breadcumb_section">
                    <!-- Breadcumb page title start -->
                    <div class="page_title">
                        <h3>Signup</h3>
                    </div>
                    <!-- Breadcumb start -->
                    <ol class="breadcrumb">
                        <li><a href="index.php">Home</a></li>
                        <li class="active">Signup</li>
                    </ol>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- ::::::::::::::::::: Breadcumb area End ::::::::::::::::::: -->

<!-- ::::::::::::::::::: Introduce Area Start ::::::::::::::::::: -->
<section class="hosting_intoduce_area section_padding_100">
    <div class="container">
        <div class="row">
            <h1>Enhorabuena ya hemos acabado!</h1>
            <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
                <!-- Dominio agregado -->
                <div class="panel panel-default">
                    <div class="panel-heading" role="tab" id="One">
                        <h4 class="panel-title">
                            <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion"
                               href="#collapse_one" aria-expanded="false" aria-controls="collapse_one">
                                <h4>Tu dominio ha sido registrado</h4>
                            </a>
                        </h4>
                    </div>
                    <div aria-expanded="false" id="collapse_one" class="panel-collapse collapse" role="tabpanel"
                         aria-labelledby="one">
                        <div class="panel-body">
                            <p>Recuerda que estamos de pruebas y el dominio no ha sido propagado por Internet :D</p>
                        </div>
                    </div>
                </div>
                <!-- Cuenta creada -->
                <div class="panel panel-default">
                    <div class="panel-heading" role="tab" id="Two">
                        <h4 class="panel-title">
                            <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion"
                               href="#collapse_two" aria-expanded="false" aria-controls="collapse_two">
                                <h4>Tu cuenta ha sido creada</h4>
                            </a>
                        </h4>
                    </div>
                    <div aria-expanded="false" id="collapse_two" class="panel-collapse collapse" role="tabpanel"
                         aria-labelledby="Two">
                        <div class="panel-body">
                            <div class="special_area_text">
                                <p>Ahora puedes acceder al panel de administración usando tus credenciales:
                                    <a href="https://mihost.ml:8082/" target="_blank">https://mihost.ml:8082/</a>
                                </p>
                                <h5>Crea nuevos buzones de correo</h5>
                                <h5>Crea nuevos usuarios FTP</h5>
                                <h5>Crea nuevas bases de datos y usuarios MySQL</h5>
                                <h5>Edita la configuración de tu sitio WEB</h5>
                                <h5>Crea y edita tus propios registros DNS.</h5>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- Servidor WEB -->
                <div class="panel panel-default">
                    <div class="panel-heading" role="tab" id="Three">
                        <h4 class="panel-title">
                            <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion"
                               href="#collapse_three" aria-expanded="false" aria-controls="collapse_three">
                                <h4>Tu espacio web ha sido creado</h4>
                            </a>
                        </h4>
                    </div>
                    <div aria-expanded="false" id="collapse_three" class="panel-collapse collapse" role="tabpanel"
                         aria-labelledby="Three">
                        <div class="panel-body">
                            <div class="special_area_text">
                                <h6>Acceso a nuestro servidor web: Apache2</h6>
                                <p>Accede a tu dominio a través de la URL: <a target="_blank"
                                                                              href="http://<?php echo $_SESSION['cart']['domain'] ?>">http://<?php echo $_SESSION['cart']['domain'] ?></a>
                                </p>
                                <h5>Cuota de disco reservado para tu
                                    web: <?php echo $account_params['limit_web_quota'] ?>MiB</h5>
                                <h5>Instalación automátizada de CMS (wordpress, prestashop...) </h5>
                                <h5>Redireccinamiento personalizable</h5>
                                <h5>Documentos de error personalizables</h5>
                                <h5>Hemos generado uno por ti, pero recuerda que puedes utilizar tu propio certificado SSL</h5>
                            </div>
                            <div class="special_area_image">
                                <img src="img/bg-pattern/" alt="">
                            </div>
                        </div>
                    </div>
                </div>
                <!-- Servidor MySQL -->
                <div class="panel panel-default">
                    <div class="panel-heading" role="tab" id="Seven">
                        <h4 class="panel-title">
                            <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion"
                               href="#collapse_seven" aria-expanded="false" aria-controls="collapse_seven">
                                <h4>Crea tus propias bases de datos desde el panel de administración</h4>
                            </a>
                        </h4>
                    </div>
                    <div aria-expanded="false" id="collapse_seven" class="panel-collapse collapse" role="tabpanel"
                         aria-labelledby="Seven">
                        <div class="panel-body">
                            <div class="special_area_text">
                                <h6>Acceso a nuestro servidor de base de datos: MariaDB.</h6>
                                <p>Una vez creadas, puedes admnistralas utilizando phpMyAdmin:
                                    <a href="http://<?php echo $_SESSION['cart']['domain'] ?>/websql">
                                        http://<?php echo $_SESSION['cart']['domain'] ?>/websql
                                    </a>
                                </p>
                                <h5>Limite de bases de datos MySQL: <?php echo $account_params['limit_database'] ?></h5>
                                <h5>Cuota de disco reservado para cada base de datos: <?php echo $account_params['limit_database_quota'] ?>MiB</h5>
                                <h5>Puedes acceder remotamente a tu base de datos utilizando:
                                    <a target="_blank" href="https://www.mysql.com/products/workbench/">
                                        MySQLWorkbench
                                    </a>
                                </h5>
                                <h5>Recuerda proteger tus datos! Desde el panel de administración puedes restringir el acceso remoto por direccion IP</h5>

                            </div>
                            <div class="special_area_image">
                                <img src="img/bg-pattern/" alt="">
                            </div>
                        </div>
                    </div>
                </div>
                <!-- Servidor FTP -->
                <div class="panel panel-default">
                    <div class="panel-heading" role="tab" id="Four">
                        <h4 class="panel-title">
                            <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion"
                               href="#collapse_four" aria-expanded="false" aria-controls="collapse_four">
                                <h4>Tu cuenta FTP ha sido creada</h4>
                            </a>
                        </h4>
                    </div>
                    <div aria-expanded="false" id="collapse_four" class="panel-collapse collapse" role="tabpanel"
                         aria-labelledby="Four">
                        <div class="panel-body">
                            <div class="special_area_text">
                                <h6>Acceso al servidor de ficheros: Pure-FTPd</h6>
                                <p>Utilizar tus credenciales y la siguiente dirección:
                                    <a target="_blank"
                                       href="ftp://<?php echo $_SESSION['cart']['domain'] ?>">ftp://<?php echo $_SESSION['cart']['domain'] ?>
                                    </a>
                                </p>
                                <h5>Gestiona el contenido del directorio web a través de una conexión cifrada FTPs</h5>
                                <h5>Utiliza un cliente FTP como <a target="_blank" href="https://filezilla-project.org">FileZilla</a></h5>
                                <h5>Tambien puedes sincronizar tu aplicación web con un IDE como <a href="https://netbeans.org">Netbeans</a></h5>
                                <h5>Ya hemos creado una por ti, pero si tu plan te lo permite puedes crear nuevas cuentas FTP desde el panel de administración.</h5>
                                <h5>Limite de usuarios FTP: <?php echo $account_params['limit_ftp_user'] ?></h5>
                            </div>
                            <div class="special_area_image">
                                <img src="img/bg-pattern/" alt="">
                            </div>
                        </div>
                    </div>
                </div>
                <!-- Servidor de correo -->
                <div class="panel panel-default">
                    <div class="panel-heading" role="tab" id="Five">
                        <h4 class="panel-title">
                            <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion"
                               href="#collapse_five" aria-expanded="false" aria-controls="collapse_five">
                                <h4>Crea nuevos buzones de correo electrónico desde el panel de administración.</h4>
                            </a>
                        </h4>
                    </div>
                    <div aria-expanded="false" id="collapse_five" class="panel-collapse collapse" role="tabpanel"
                         aria-labelledby="Five">
                        <div class="panel-body">
                            <div class="special_area_text">
                                <h6>Acceso al servidor de correo: Postfix+Dovecot</h6>
                                <p>Tu dominio ahora cuenta con su propio servicio de correo:
                                    <a target="_blank" href="smtp://mail.<?php echo $_SESSION['cart']['domain'] ?>">mail.<?php echo $_SESSION['cart']['domain'] ?></a>
                                    <br> Recuerda! Debes utiliza está dirección para configurar tu cliente de correo.
                                </p>
                                <h5>Cuota de disco reservado para tus cuentas de correo:
                                    <?php echo $account_params['limit_mailquota'] ?>MiB</h5>
                                <h5>Limite de buzones de correo: <?php echo $account_params['limit_mailbox'] ?></h5>
                                <h5>Podrás acceder a tus buzones utilizando un navegador web a través de la url:
                                    <a target="_blank"
                                       href="https://<?php echo $_SESSION['cart']['domain'] ?>/webmail"><?php echo $_SESSION['cart']['domain'] ?>/webmail
                                    </a>
                                </h5>
                                <h5>Si lo prefieres, también puedes utilizar una aplicación de escritorio como:
                                    <a target="_blank" href="https://www.mozilla.org/es/thunderbird/">
                                        Thunderbird
                                    </a>
                                </h5>
                            </div>
                            <div class="special_area_image">
                                <img src="img/bg-pattern/" alt="">
                            </div>
                        </div>
                    </div>
                </div>
                <!-- place order button -->
                <br>
                <div class="place_order_button">
                    <button type="button" onclick="window.open('https://mihost.ml:8082/','_blank');"
                            class="btn btn-default">Acceder al panel de administracion
                    </button>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- ::::::::::::::::::: Introduce area End ::::::::::::::::::: -->

<!-- ::::::::::::::::::: Call to action Area Start ::::::::::::::::::: -->
<div class="call_to_action section_padding_60">
    <div class="container">
        <div class="row">
            <div class="col-xs-12">
                <!-- call to action text -->
                <div class="call_to wow fadeInUp" data-wow-delay="0.2s">
                    <h3>¿Quieres ver la petición/repuesta de la API?</h3>
                    <div class="call_to_action_button">
                        <a href="#apioutput">Pues claro!</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- ::::::::::::::::::: Call to action Area End ::::::::::::::::::: -->

<!-- ::::::::::::::::::: Partner Area Start ::::::::::::::::::: -->
<div class="partner_area">
    <div class="container">
        <div class="row">
            <div class="col-xs-12">
                <div class="partners_thumbs slide">
                    <!-- Single Partner Logo Area -->
                    <div class="single_partner_area">
                        <a href="">
                            <div class="single_partner_thumb">
                                <img src="img/partners-img/1.png" alt="">
                            </div>
                        </a>
                    </div>
                    <!-- Single Partner Logo Area -->
                    <div class="single_partner_area">
                        <a href="">
                            <div class="single_partner_thumb">
                                <img src="img/partners-img/2.png" alt="">
                            </div>
                        </a>
                    </div>
                    <!-- Single Partner Logo Area -->
                    <div class="single_partner_area">
                        <a href="">
                            <div class="single_partner_thumb">
                                <img src="img/partners-img/3.png" alt="">
                            </div>
                        </a>
                    </div>
                    <!-- Single Partner Logo Area -->
                    <div class="single_partner_area">
                        <a href="">
                            <div class="single_partner_thumb">
                                <img src="img/partners-img/4.png" alt="">
                            </div>
                        </a>
                    </div>
                    <!-- Single Partner Logo Area -->
                    <div class="single_partner_area">
                        <a href="">
                            <div class="single_partner_thumb">
                                <img src="img/partners-img/5.png" alt="">
                            </div>
                        </a>
                    </div>
                    <!-- Single Partner Logo Area -->
                    <div class="single_partner_area">
                        <a href="">
                            <div class="single_partner_thumb">
                                <img src="img/partners-img/6.png" alt="">
                            </div>
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- ::::::::::::::::::: Partner Area End ::::::::::::::::::: -->

<!-- ::::::::::::::::::: Footer Area Start ::::::::::::::::::: -->
<?php include("./php/include/footer.php"); ?>
<!-- ::::::::::::::::::: Footer Area End ::::::::::::::::::: -->

<!-- ::::::::::::::::::: All jQuery Plugins ::::::::::::::::::: -->

<!-- jQuery (necessary for all JavaScript plugins) -->
<script src="js/jquery-2.2.4.min.js"></script>

<!-- Bootstrap 3.3.7 js -->
<script src="js/bootstrap.min.js"></script>

<!-- Owl-carousel js -->
<script src="js/owl.carousel.min.js"></script>

<!-- Ajax Contact js -->
<script src="js/ajax-contact.js"></script>

<!-- Meanmenu js -->
<script src="js/meanmenu.js"></script>

<!-- Waypoint js -->
<script src="js/waypoints.min.js"></script>

<!-- Counterup js -->
<script src="js/counterup.min.js"></script>

<!-- jQuery easing js -->
<script src="js/jquery.easing.1.3.js"></script>

<!-- ScrollUp js -->
<script src="js/jquery.scrollUp.js"></script>

<!-- WoW js -->
<script src="js/wow.min.js"></script>

<!-- Active js -->
<script src="js/active.js"></script>

</body>


<?php
session_start();
if (!isset($_SESSION['cart']['plan']) || !isset($_SESSION['cart']['domain']) || !isset($_POST)) {
    header("Locate: ./index.php");
}
if ($_SERVER['REQUEST_METHOD'] === 'POST') {
    // prevent XSS
    $_POST = filter_input_array(INPUT_POST, FILTER_SANITIZE_STRING);
    echo '<pre id="apioutput">';
    echo htmlspecialchars(print_r($_POST, true));

    // Client
    require './soap/soap_config.php';
    $client = new SoapClient(null, array('location' => $soap_location,
        'uri' => $soap_uri,
        'trace' => 1,
        'stream_context' => stream_context_create(array('ssl' => array('verify_peer' => false, 'verify_peer_name' => false))),
        'exceptions' => 1));

    try {
        if ($session_id = $client->login($soap_username, $soap_password)) {
            echo 'SOAP Logged successfull: Session ID:' . $session_id . '<br />';
        }
        $client_id = $client->client_add($session_id, $reseller_id, $account_params);
        echo "CLIENT_ID: " . $client_id . "<br>";
        print_r($params);
        // DNS - SOA  Agregando zona al cliente
        $params = array(
            'server_id' => 1,
            'origin' => $_SESSION['cart']['domain'] . '.',
            'ns' => 'ns1.mihost.ml.',
            'mbox' => 'zonemaster.' . $_SESSION['cart']['domain'] . '.',
            'serial' => date("Ymd"),
            'refresh' => '7200',
            'retry' => '540',
            'expire' => '604800',
            'minimum' => '3600',
            'ttl' => '3600',
            'active' => 'y',
            'xfer' => '',
            'also_notify' => '',
            'update_acl' => '',
        );
        $zone_id = $client->dns_zone_add($session_id, $client_id, $params);
        echo "DNS_SOA_ID: " . $zone_id . "<br>";
        print_r($params);

        // DNS - NS  
        $params = array(
            'server_id' => 1,
            'zone' => $zone_id,
            'name' => $_SESSION['cart']['domain'] . '.',
            'type' => 'ns',
            'data' => 'ns1.mihost.ml.',
            'aux' => '0',
            'ttl' => '3600',
            'active' => 'y',
            'serial' => date("Ymd"),
        );
        $client->dns_ns_add($session_id, $client_id, $params);

        // DNS - A - domain.tld - 192.168.108.5
        $params = array(
            'server_id' => 1,
            'zone' => $zone_id,
            'name' => $_SESSION['cart']['domain'] . '.',
            'type' => 'a',
            'data' => '192.168.108.5',
            'aux' => '0',
            'ttl' => '3600',
            'active' => 'y',
            'serial' => date("Ymd")
        );
        $client->dns_a_add($session_id, $client_id, $params);


        // DNS - A - mail.domain.tld - 192.168.108.5
        $params = array(
            'server_id' => 1,
            'zone' => $zone_id,
            'name' => 'mail',
            'type' => 'a',
            'data' => '192.168.108.5',
            'aux' => '0',
            'ttl' => '3600',
            'active' => 'y',
            'serial' => date("Ymd")
        );
        $client->dns_a_add($session_id, $client_id, $params);

        // DNS - A - www.domain.tld - 192.168.108.5
        $params = array(
            'server_id' => 1,
            'zone' => $zone_id,
            'name' => 'www',
            'type' => 'a',
            'data' => '192.168.108.5',
            'aux' => '0',
            'ttl' => '3600',
            'active' => 'y',
            'serial' => date("Ymd")
        );
        $client->dns_a_add($session_id, $client_id, $params);

        // DNS - MX - mail.domain.tld
        $params = array(
            'server_id' => 1,
            'zone' => $zone_id,
            'name' => $_SESSION['cart']['domain'] . '.',
            'type' => 'mx',
            'data' => 'mail.' . $_SESSION['cart']['domain'] . '.',
            'aux' => '10',
            'ttl' => '3600',
            'active' => 'y',
            'serial' => date("Ymd")
        );
        $client->dns_mx_add($session_id, $client_id, $params);

        // DNS - TXT - SPF
        $params = array(
            'server_id' => 1,
            'zone' => $zone_id,
            'name' => $_SESSION['cart']['domain'] . '.',
            'type' => 'txt',
            'data' => 'v=spf1 mx a ~all',
            'aux' => '0',
            'ttl' => '3600',
            'active' => 'y',
            'serial' => date("Ymd")
        );
        $client->dns_txt_add($session_id, $client_id, $params);

        // MAIL - DOMAIN
        $params = array(
            'server_id' => 1,
            'domain' => $_SESSION['cart']['domain'],
            'active' => 'y'
        );
        $mail_domain_id = $client->mail_domain_add($session_id, $client_id, $params);

        echo "MAIL_DOMAIN_ID: " . $mail_domain_id . "<br>";
        print_r($params);

        // WEB - DOMAIN
        $web_params = array(
            'server_id' => 1,
            'ip_address' => '192.168.108.5',
            'http_port' => 80,
            'https_port' => 443,
            'domain' => $_SESSION['cart']['domain'],
            'type' => 'vhost',
            'parent_domain_id' => 0,
            'vhost_type' => 'name',
            'hd_quota' => $account_params['limit_web_quota'],
            'traffic_quota' => $account_params['limit_traffic_quota'],
            'cgi' => 'n',
            'ssi' => 'n',
            'ruby' => 'n',
            'python' => 'n',
            'perl' => 'n',
            'suexec' => 'y',
            'errordocs' => 1,
            'is_subdomainwww' => 1,
            'subdomain' => 'www',
            'php' => 'fast-cgi',
            'redirect_type' => '',
            'redirect_path' => '',
            'seo_redirect' => 'non_www_to_www', // non_www_to_www
            'rewrite_to_https' => 'y',
            'ssl' => 'y',
            'ssl_state' => '',
            'ssl_locality' => '',
            'ssl_organisation' => 'miHost, Inc',
            'ssl_organisation_unit' => 'miHost Self-Signed Certificates',
            'ssl_country' => 'ES',
            'ssl_domain' => '*.' . $_SESSION['cart']['domain'],
            'ssl_request' => '',
            'ssl_key' => '',
            'ssl_cert' => '',
            'ssl_bundle' => '',
            'ssl_action' => 'create',
            'stats_password' => $_POST['password'],
            'stats_type' => 'awstats',
            'allow_override' => 'All',
            'apache_directives' => '',
            'pm_max_requests' => 0,
            'pm_process_idle_timeout' => 10,
            'custom_php_ini' => '',
            'backup_interval' => '',
            'backup_copies' => 1,
            'active' => 'y',
            'log_retention' => 30,
            'traffic_quota_lock' => 'n'
        );
        $web_domain_id = $client->sites_web_domain_add($session_id, $client_id, $web_params, $readonly = false);
        echo "Web Domain ID: " . $web_domain_id . "<br>";
        $web_domain_record = $client->sites_web_domain_get($session_id, $web_domain_id);
        print_r($web_domain_record);

        // ADD FTP USER
        $params = array(
            'server_id' => 1,
            'parent_domain_id' => $web_domain_id,
            'username' => $_POST['username'],
            'password' => $_POST['password'],
            'quota_size' => -1,
            'active' => 'y',
            'uid' => $web_domain_record['system_user'],
            'gid' => $web_domain_record['system_group'],
            'dir' => $web_domain_record['document_root'],
            'quota_files' => -1,
            'ul_ratio' => -1,
            'dl_ratio' => -1,
            'ul_bandwidth' => -1,
            'dl_bandwidth' => -1
        );
        print_r($params);
        $client->sites_ftp_user_add($session_id, $client_id, $params);


    } catch (SoapFault $e) {
        echo $client->__getLastResponse();
        die('SOAP Error: ' . $e->getMessage());
    }

    echo '</pre>';
}
