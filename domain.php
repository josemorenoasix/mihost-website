<!DOCTYPE html>
<html lang="es">

<head>
    <meta charset="UTF-8">
    <meta name="description" content="">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->

    <!-- Title  -->
    <title>Domain - miHost Hosting Servicese</title>

    <!-- Favicon  -->
    <link rel="shortcut icon" href="img/core-img/favicon.ico">

    <!-- ::::::::::::::::::: All CSS Files ::::::::::::::::::: -->

    <!-- Style css -->
    <link rel="stylesheet" href="style.css">

    <!-- Responsive css -->
    <link rel="stylesheet" href="css/responsive.css">

    <!--[if IE]>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/html5shiv/3.7.3/html5shiv.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->

</head>

<body>
<!-- ::::::::::::::::::: include login.php ::::::::::::::::::: -->
<?php include("./php/include/login.php"); ?>
<!-- ::::::::::::::::::: Header End ::::::::::::::::::: -->

<!-- ::::::::::::::::::: include header.php ::::::::::::::::::: -->
<?php include("./php/include/header.php"); ?>
<!-- ::::::::::::::::::: Header End ::::::::::::::::::: -->

<!-- ::::::::::::::::::: Breadcumb area start ::::::::::::::::::: -->
<section class="breadcumb_area">
    <div class="container">
        <div class="row">
            <div class="col-xs-12">
                <div class="breadcumb_section">
                    <!-- Breadcumb page title start -->
                    <div class="page_title">
                        <h3>Domain</h3>
                    </div>
                    <!-- Breadcumb start -->
                    <ol class="breadcrumb">
                        <li><a href="index.php">Home</a></li>
                        <li class="active">Domain</li>
                    </ol>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- ::::::::::::::::::: Breadcumb area End ::::::::::::::::::: -->
<?php
// prevent XSS
$_GET = filter_input_array(INPUT_GET, FILTER_SANITIZE_STRING);
$_POST = filter_input_array(INPUT_POST, FILTER_SANITIZE_STRING);
//
if ($_SERVER['REQUEST_METHOD'] === 'POST') {
    //var_dump($_POST);
    $regex = '/^(([a-zA-Z]{1})|([a-zA-Z]{1}[a-zA-Z]{1})|([a-zA-Z]{1}[0-9]{1})|([0-9]{1}[a-zA-Z]{1})|([a-zA-Z0-9][a-zA-Z0-9-_]{1,61}[a-zA-Z0-9]))\.([a-zA-Z]{2,6}|[a-zA-Z0-9-]{2,30}\.[a-zA-Z]{2,3})$/';
    if (isset ($_POST['searchdomain'])) {
        preg_match($regex, $_POST['searchdomain'], $clean_domain);
        //var_dump($clean_domain);
        $domain_search = $clean_domain[0];
    } else {
        echo "POST searchdomain vacio.";
    }

    echo "<br>";
    require_once './src/Unirest.php';
    echo 'Unirest\Request::get("https://domainr.p.mashape.com/v2/status?mashape-key=&domain=' . $domain_search . '",';
    $json_response = Unirest\Request::get("https://domainr.p.mashape.com/v2/status?mashape-key=&domain=$domain_search",
        array(
            "X-Mashape-Key" => "xRhYeXxuJbmsh6eU3dTr8MwsMbbrp1eKcKqjsn1J8BTtBoYjBj",
            "Accept" => "application/json"
        )
    );
    $response = json_decode(json_encode($json_response), true);
    echo "<br>";
    echo "<pre>";
    var_dump($response);
    echo "</pre>";


}


?>
<!-- ::::::::::::::::::: Domain Search Box Area Start ::::::::::::::::::: -->
<section id="search" class="domain_search_area domain_page section_padding_100">
    <div class="container">
        <div class="row">
            <div class="col-md-10 col-md-offset-1">
                <div class="search_text">
                    <h4>Encuentra el dominio que buscas:</h4>
                </div>
                <form method="POST" action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]); ?>" class="domain_form">
                    <div class="form-group">
                        <input type="search" name="searchdomain" class="form-control search_box"
                               placeholder="domain.tld">
                    </div>
                    <button type="submit" class="btn btn-default submit_btn">Search</button>
                </form>

            </div>
        </div>
    </div>

    <div class="container">
        <div class="row">
            <div class="col-xs-12">
                <?php
                if (!empty($response['body']['status'][0]['summary'])) {
                    if ($response['body']['status'][0]['summary'] == 'inactive') {
                        ?>
                        <div class="success_failure_result m_top_30">
                            <div class="notification">
                                <h3>Enhorabuena! <span><?php echo $response['body']['status'][0]['domain'] ?></span>
                                    está disponible.</h3>
                            </div>
                            <div class="add_to_cart_btn">
                                <a class="btn btn-default" href="#" role="button">Adquirirlo!</a>
                            </div>
                        </div>
                    <?php } else { ?>
                        <div class="success_failure_result m_top_30">
                            <div class="notification fail">
                                <h3>Lamentablemente <span><?php echo $response['body']['status'][0]['domain'] ?></span>
                                    ya esta en uso.</h3>
                            </div>
                            <div class="add_to_cart_btn">
                                <a class="btn btn-default disabled" href="cart.html" role="button">Add to Cart</a>
                            </div>
                        </div>
                    <?php }
                } ?>
            </div>
        </div>
    </div>
</section>
<!-- ::::::::::::::::::: Domain Search Box Area End ::::::::::::::::::: -->

<!-- ::::::::::::::::::: Call to action Area Start ::::::::::::::::::: -->
<div class="call_to_action section_padding_60">
    <div class="container">
        <div class="row">
            <div class="col-xs-12">
                <!-- call to action text -->
                <div class="call_to wow fadeInUp" data-wow-delay="0.2s">
                    <h3>Selecciona el plan que mejor se adapte a tus necesidades!</h3>
                    <div class="call_to_action_button">
                        <a href="index.php#plans">Elige</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- ::::::::::::::::::: Call to action Area End ::::::::::::::::::: -->

<!-- ::::::::::::::::::: Partner Area Start ::::::::::::::::::: -->
<div class="partner_area">
    <div class="container">
        <div class="row">
            <div class="col-xs-12">
                <div class="partners_thumbs slide">
                    <!-- Single Partner Logo Area -->
                    <div class="single_partner_area">
                        <a href="">
                            <div class="single_partner_thumb">
                                <img src="img/partners-img/1.png" alt="">
                            </div>
                        </a>
                    </div>
                    <!-- Single Partner Logo Area -->
                    <div class="single_partner_area">
                        <a href="">
                            <div class="single_partner_thumb">
                                <img src="img/partners-img/2.png" alt="">
                            </div>
                        </a>
                    </div>
                    <!-- Single Partner Logo Area -->
                    <div class="single_partner_area">
                        <a href="">
                            <div class="single_partner_thumb">
                                <img src="img/partners-img/3.png" alt="">
                            </div>
                        </a>
                    </div>
                    <!-- Single Partner Logo Area -->
                    <div class="single_partner_area">
                        <a href="">
                            <div class="single_partner_thumb">
                                <img src="img/partners-img/4.png" alt="">
                            </div>
                        </a>
                    </div>
                    <!-- Single Partner Logo Area -->
                    <div class="single_partner_area">
                        <a href="">
                            <div class="single_partner_thumb">
                                <img src="img/partners-img/5.png" alt="">
                            </div>
                        </a>
                    </div>
                    <!-- Single Partner Logo Area -->
                    <div class="single_partner_area">
                        <a href="">
                            <div class="single_partner_thumb">
                                <img src="img/partners-img/6.png" alt="">
                            </div>
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- ::::::::::::::::::: Partner Area End ::::::::::::::::::: -->

<!-- ::::::::::::::::::: Footer Area Start ::::::::::::::::::: -->
<?php include("footer.php"); ?>
<!-- ::::::::::::::::::: Footer Area End ::::::::::::::::::: -->

<!-- ::::::::::::::::::: All jQuery Plugins ::::::::::::::::::: -->

<!-- jQuery (necessary for all JavaScript plugins) -->
<script src="js/jquery-2.2.4.min.js"></script>

<!-- Bootstrap 3.3.7 js -->
<script src="js/bootstrap.min.js"></script>

<!-- Owl-carousel js -->
<script src="js/owl.carousel.min.js"></script>

<!-- Ajax Contact js -->
<script src="js/ajax-contact.js"></script>

<!-- Meanmenu js -->
<script src="js/meanmenu.js"></script>

<!-- Waypoint js -->
<script src="js/waypoints.min.js"></script>

<!-- Counterup js -->
<script src="js/counterup.min.js"></script>

<!-- jQuery easing js -->
<script src="js/jquery.easing.1.3.js"></script>

<!-- ScrollUp js -->
<script src="js/jquery.scrollUp.js"></script>

<!-- WoW js -->
<script src="js/wow.min.js"></script>

<!-- Active js -->
<script src="js/active.js"></script>

</body>

</html>

