<!-- ::::::::::::::::::: Header Start ::::::::::::::::::: -->
<header class="header_area">
    <!-- Top Header Area Start -->
    <div class="top_header_area">
        <div class="container">
            <div class="row">
                <div class="col-xs-12 hidden-sm hidden-md hidden-lg">
                    <!--  This section only for mobile version -->
                    <div class="mobile_header">
                        <!-- Support Text -->
                        <div class="support_text">
                            <p>24/7 Support (+34) 644 000 111</p>
                        </div>
                    </div>
                </div>
            </div>

            <div class="row hidden-xs">
                <div class="col-sm-12 col-xs-6">
                    <div class="top_right_side_content pull-right">
                        <!-- Support Text -->
                        <div class="support_text">
                            <p>24/7 Soporte (+34) 644 000 111</p>
                        </div>
                        <!-- Need Help -->
                        <div class="need_help">
                            <a href="#">Ayuda</a>
                        </div>
                        <!-- Login Register Area -->
                        <div class="login_register">
                            <div class="login">
                                <i class="icon-user" aria-hidden="true"></i>
                                <a data-toggle="modal" href="https://mihost.ml:8082">Login</a>
                                <!-- <a data-target="#login" data-toggle="modal" href="#">Login</a> -->
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Top Header Area End -->

    <!-- Main Header Area Start -->
    <div class="main_header_area clearfix">
        <div class="container">
            <div class="row">
                <div class="col-sm-2 col-xs-12">
                    <!-- Logo Area:: For better view in all device please use logo image max-width 180px*30px -->
                    <div class="logo_area">
                        <a href="index.php"><img src="img/core-img/logo.png" alt=""></a>
                    </div>

                    <div class="mobile_cart_login_area pull-right">
                        <!-- This Cart Section is only for Mobile Version -->
                        <div class="cart_area mobile hidden-sm hidden-md hidden-lg">
                            <a href="cart.php"><i class="icon-shopping-cart" aria-hidden="true"></i>
                                <div class="cart_total"><p>2</p></div>
                            </a>
                        </div>

                        <!-- Login Register Area -->
                        <div class="login_register hidden-sm hidden-md hidden-lg">
                            <div class="login">
                                <i class="icon-user" aria-hidden="true"></i>
                                <a data-target="#login" data-toggle="modal" href="https://mihost.ml:8082">Login</a>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-sm-10 col-xs-12">
                    <!-- Main menu area start -->
                    <div class="main_menu_area">
                        <nav id="navbar">
                            <ul id="nav">
                                <li class="current-menu-item"><a href="index.php">Home</a></li>
                                <li><a href="domain.php">dominio</a></li>
                                <!--
                                <li><a href="#">planes <i class="fa fa-angle-down" aria-hidden="true"></i></a>
                                    <ul class="sub-menu">
                                        <li><a href="web-hosting.html">web hosting</a></li>
                                        <li><a href="cloud-hosting.html">cloud hosting</a></li>
                                        <li><a href="vpn-hosting.html">vpn hosting</a></li>
                                        <li><a href="shared-hosting.html">shared hosting</a></li>
                                        <li><a href="wordpress-hosting.html">WordPress hosting</a></li>
                                    </ul>

                                </li>


                                <li class="dropdown-megamenu hidden-xs"><a href="#">pages <i class="fa fa-angle-down" aria-hidden="true"></i></a>
                                    <ul class="mega-menu clearfix">
                                        <li class="mega-menu-col">
                                            <ul class="mega-menu-row">
                                                <li><a href="index.php">Home</a></li>
                                                <li><a href="about-us.html">about us</a></li>
                                                <li><a href="domain-success.html">Domain Success</a></li>
                                                <li><a href="domain-failure.html">Domain Failure</a></li>
                                                <li><a href="web-hosting.html">web hosting</a></li>
                                                <li><a href="cloud-hosting.html">cloud hosting</a></li>
                                                <li><a href="vpn-hosting.html">vpn hosting</a></li>
                                            </ul>
                                        </li>
                                        <li class="mega-menu-col">
                                            <ul class="mega-menu-row">
                                                <li><a href="shared-hosting.html">Shared hosting</a></li>
                                                <li><a href="wordpress-hosting.html">WordPress hosting</a></li>
                                                <li><a href="register.html">Register</a></li>
                                                <li><a href="support.html">Help</a></li>
                                                <li><a href="contact.html">contact</a></li>
                                                <li><a href="404.html">404 page</a></li>
                                                <li><a href="page.html">Empty Page</a></li>
                                            </ul>
                                        </li>
                                        <li class="mega-menu-col">
                                            <ul class="mega-menu-row">
                                                <li><a href="cart.php">cart page</a></li>
                                                <li><a href="checkout.html">checkout page</a></li>
                                                <li><a href="blog-sidebar.html">blog page</a></li>
                                                <li><a href="single.html">single blog page</a></li>
                                                <li><a href="pricing-plan.html">Price plan</a></li>
                                                <li><a href="privacy-policy.html">Privacy Policy</a></li>
                                                <li><a href="coming-soon.html">Coming Soon</a></li>
                                            </ul>
                                        </li>
                                    </ul>
                                </li>
                                -->
                                <li><a href="#">Soporte</a></li>
                                <li><a href="#">Blog <i class="fa fa-angle-down" aria-hidden="true"></i></a></li>
                                <li><a href="#">Contacta</a></li>
                            </ul>
                        </nav>
                        <!-- Cart Section -->
                        <div class="cart_area hidden-xs">
                            <a href="cart.php"><i class="icon-shopping-cart icon-large" aria-hidden="true"></i>
                                <div class="cart_total"><p>2</p></div>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Main Header Area End -->
</header>
<!-- ::::::::::::::::::: Header End ::::::::::::::::::: -->

<?php
session_start();
