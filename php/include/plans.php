<?php
// Establece permisos segun el paquete seleccionado

switch ($_SESSION['cart']['plan']) {
    case "basic":
        $reseller_id = 0; // diferente de cero solo para admins o resellers
        $account_params = array(
            // datos personales
            'company_name' => $_POST['companyname'],
            'contact_name' => $_POST['contactname'],
            'customer_no' => $_POST['customernumber'],
            'street' => $_POST['street'],
            'zip' => $_POST['zip'],
            'city' => $_POST['city'],
            'state' => $_POST['province'],
            'country' => $_POST['country'],
            'telephone' => $_POST['telephone'],
            'mobile' => '',
            'fax' => '',
            'email' => $_POST['email'],
            'internet' => '',
            'bank_account_owner' => $_POST['cc_name'],
            'bank_account_number' => $_POST['cc_number'],
            'paypal_email' => $_POST['paypal'],
            'username' => $_POST['username'],
            'password' => $_POST['password'],
            'language' => 'es',
            'usertheme' => 'default',
            'template_master' => 0,
            'template_additional' => '',
            'created_at' => time(),
            'added_by' => 'remote_api',

            // Parametros entorno multi-server
            // ID del servidor dns
            'default_dnsserver' => 1,
            'dns_servers' => 1,
            // ID del servidor web
            'default_webserver' => 1,
            'web_servers' => 1,
            'limit_web_ip' => '',
            // ID del servidor MySQL
            'default_dbserver' => 1,
            'db_servers' => 1,
            // ID del servidor mail
            'default_mailserver' => 1,
            'mail_servers' => 1,

            // Servicio DNS
            'limit_dns_zone' => 1,
            'limit_dns_slave_zone' => 0,
            'limit_dns_record' => 25,

            // Servicio Web
            'limit_web_domain' => 1,            // Limite de dominios
            // Limitaciones por dominio
            'limit_web_subdomain' => 5,        // Limite de subdominios
            'limit_web_aliasdomain' => 0,    // Limite de alias entre dominios: domain.com --> domain.tld
            'limit_web_quota' => 2048,        // 2GiB Cuota de disco en MB
            'limit_traffic_quota' => -1,        // Limite de trafico web mensual en MB
            'force_suexec' => 'y',            // Forzar uso suexec para aumentar seguridad https://wiki.archlinux.org/index.php/Apache,_suEXEC_and_Virtual_Hosts
            'limit_hterror' => 'y',            // Documentos error personalizados
            'limit_ssl' => 'y',                // Permite el uso del protocolo HTTPs
            'limit_ssl_letsencrypt' => 'n',    // Cambiar a yes en entorno real
            'limit_wildcard' => 'y',            // Certificado SSL con wildcard para subdominios.
            'web_php_options' => 'no,mod',    // Interpretes PHP 'no,fast-cgi,cgi,mod,suphp,php-fpm,hhvm'
            'limit_cgi' => 'n',                // Soporte CGI
            'limit_ssi' => 'n',                // Soporte SSI (Server Side Includes)
            'limit_perl' => 'n',                // Soporte Perl (Server Side Includes)
            'limit_ruby' => 'n',                // Soporte Ruby
            'limit_python' => 'n',            // Soporte Python
            'limit_ftp_user' => 1,            // Usuarios FTP
            'limit_shell_user' => 0,            // Usuarios SSH
            'ssh_chroot' => 'jailkit',        // Jaula acceso SSH (no, jailkit)
            'limit_webdav_user' => 0,        // Ususarios WebDav (permite montar unidades de red)
            'limit_backup' => 'n',            // Permite backups
            'limit_aps' => '1',                // Limite de aplicaciones empaquetadas (APS): wordpress, prestashop...

            // Servicio MySQL					// 1 x 512MiB database
            'limit_database' => 1,            // Limite de bases de datos
            'limit_database_user' => 1,        // Limite de usuarios mysql
            'limit_database_quota' => 512,    // Cuota de disco reservada por base de datos

            // Parametros del Servicio Mail
            'limit_maildomain' => 1,            // Limite de dominios
            // Limitaciones por dominio
            'limit_mailquota' => 1024,        // 512MiB Cuota de disco reservado mail (independiente de otras cuotas)
            'limit_mailbox' => 2,            // 2 Buzones de correo de 256MiB
            'limit_mailalias' => 5,            // Alias email (info@midomino.com --> soporte@midomino.com)
            'limit_mailcatchall' => 1,        // Alias para recoger envios fallidos al dominio (catchall@midomino.com)
            'limit_mailaliasdomain' => 0,    // Alias de dominio (midominio.com <--> midominio.es).
            'limit_mailforward' => 1,        // Reenvios servidor externo: user@domain.tld -> user@gmail.com
            'limit_fetchmail' => 1,            // Download servidor externo: user@gmail.com -> user@domain.tld
            // No se limita el uso de listas negras o filtros spam a ningun tipo de usuario.
            'limit_mailfilter' => -1,
            'limit_spamfilter_wblist' => -1,
            'limit_spamfilter_user' => -1,
            'limit_spamfilter_policy' => -1
        );
        break;
    case "standard":
        $reseller_id = 0; // this id has to be 0 if the client shall not be assigned to admin or if the client is a reseller
        $account_params = array(
            // datos personales
            'company_name' => $_POST['companyname'],
            'contact_name' => $_POST['contactname'],
            'customer_no' => $_POST['customernumber'],
            'street' => $_POST['street'],
            'zip' => $_POST['zip'],
            'city' => $_POST['city'],
            'state' => $_POST['province'],
            'country' => $_POST['country'],
            'telephone' => $_POST['telephone'],
            'mobile' => '',
            'fax' => '',
            'email' => $_POST['email'],
            'internet' => '',
            'bank_account_owner' => $_POST['cc_name'],
            'bank_account_number' => $_POST['cc_number'],
            'paypal_email' => $_POST['paypal'],
            'username' => $_POST['username'],
            'password' => $_POST['password'],
            'language' => 'es',
            'usertheme' => 'default',
            'template_master' => 0,
            'template_additional' => '',
            'created_at' => time(),
            'added_by' => 'remote_api',

            // Parametros entorno multi-server
            // ID del servidor dns
            'default_dnsserver' => 1,
            'dns_servers' => 1,
            // ID del servidor web
            'default_webserver' => 1,
            'web_servers' => 1,
            'limit_web_ip' => '',
            // ID del servidor MySQL
            'default_dbserver' => 1,
            'db_servers' => 1,
            // ID del servidor mail
            'default_mailserver' => 1,
            'mail_servers' => 1,

            // Servicio DNS
            'limit_dns_zone' => 1,
            'limit_dns_slave_zone' => 0,
            'limit_dns_record' => 50,

            // Servicio Web
            'limit_web_domain' => 1,            // Limite de dominios
            // Limitaciones por dominio
            'limit_web_subdomain' => 12,        // Limite de subdominios
            'limit_web_aliasdomain' => 6,    // Limite de alias entre dominios: domain.com --> domain.tld
            'limit_web_quota' => 10240,        // 10GiB Cuota de disco en MB
            'limit_traffic_quota' => -1,        // -1 Trafico web mensual en MB
            'force_suexec' => 'y',            // Forzado suexec para aumentar seguridad https://wiki.archlinux.org/index.php/Apache,_suEXEC_and_Virtual_Hosts
            'limit_hterror' => 'y',            // Documentos error personalizados
            'limit_ssl' => 'y',                // Permite el uso del protocolo HTTPs
            'limit_ssl_letsencrypt' => 'n',    // Cambiar a yes en entorno real
            'limit_wildcard' => 'y',            // Certificado SSL con wildcard para subdominios.
            'web_php_options' => 'no,fast-cgi,cgi,mod,suphp,php-fpm,hhvm',
            'limit_cgi' => 'y',                // Soporte CGI
            'limit_ssi' => 'y',                // Soporte SSI (Server Side Includes)
            'limit_perl' => 'y',                // Soporte Perl (Server Side Includes)
            'limit_ruby' => 'y',                // Soporte Ruby
            'limit_python' => 'y',            // Soporte Python
            'limit_ftp_user' => 5,            // Usuarios FTP
            'limit_shell_user' => 1,            // Usuarios SSH
            'ssh_chroot' => 'jailkit',        // Jaula acceso SSH (no, jailkit)
            'limit_webdav_user' => 5,        // Ususarios WebDav (permite montar unidades de red)
            'limit_backup' => 'y',            // Permite backups
            'limit_aps' => '3',                // Limite de aplicaciones empaquetadas (APS): wordpress, prestashop...

            // Servicio MySQL					// 2 x 1GiB database
            'limit_database' => 2,            // Limite de bases de datos
            'limit_database_user' => 10,        // Limite de usuarios mysql
            'limit_database_quota' => 1024,    // Cuota de disco reservada por base de datos

            // Parametros del Servicio Mail
            'limit_maildomain' => 1,            // Limite de dominios
            // Limitaciones por dominio
            'limit_mailquota' => 2560,        // 2,5GiB Cuota de disco reservado mail (independiente de otras cuotas)
            'limit_mailbox' => 5,            // 5 Buzones de correo de 512MiB
            'limit_mailalias' => 10,            // Alias email (info@midomino.com --> soporte@midomino.com)
            'limit_mailcatchall' => 1,        // Alias recoge-todo (catchall@midomino.com)
            'limit_mailaliasdomain' => 6,    // Alias de dominio (midominio.com <--> midominio.es).
            'limit_mailforward' => 5,        // Reenvios servidor externo: user@domain.tld -> user@gmail.com
            'limit_fetchmail' => 5,            // Download servidor externo: user@gmail.com -> user@domain.tld
            // No se limita el uso de listas negras o filtros spam a ningun tipo de usuario.
            'limit_mailfilter' => -1,
            'limit_spamfilter_wblist' => -1,
            'limit_spamfilter_user' => -1,
            'limit_spamfilter_policy' => -1
        );
        break;
    case "business":
        $reseller_id = 0; // this id has to be 0 if the client shall not be assigned to admin or if the client is a reseller
        $account_params = array(
            // datos personales
            'company_name' => $_POST['companyname'],
            'contact_name' => $_POST['contactname'],
            'customer_no' => $_POST['customernumber'],
            'street' => $_POST['street'],
            'zip' => $_POST['zip'],
            'city' => $_POST['city'],
            'state' => $_POST['province'],
            'country' => $_POST['country'],
            'telephone' => $_POST['telephone'],
            'mobile' => '',
            'fax' => '',
            'email' => $_POST['email'],
            'internet' => '',
            'bank_account_owner' => $_POST['cc_name'],
            'bank_account_number' => $_POST['cc_number'],
            'paypal_email' => $_POST['paypal'],
            'username' => $_POST['username'],
            'password' => $_POST['password'],
            'language' => 'es',
            'usertheme' => 'default',
            'template_master' => 0,
            'template_additional' => '',
            'created_at' => time(),
            'added_by' => 'remote_api',

            // Parametros entorno multi-server
            // ID del servidor dns
            'default_dnsserver' => 1,
            'dns_servers' => 1,
            // ID del servidor web
            'default_webserver' => 1,
            'web_servers' => 1,
            'limit_web_ip' => '',
            // ID del servidor MySQL
            'default_dbserver' => 1,
            'db_servers' => 1,
            // ID del servidor mail
            'default_mailserver' => 1,
            'mail_servers' => 1,

            // Servicio DNS
            'limit_dns_zone' => 1,
            'limit_dns_slave_zone' => 0,
            'limit_dns_record' => 100,

            // Servicio Web
            'limit_web_domain' => 1,            // Limite de dominios
            // Limitaciones por dominio
            'limit_web_subdomain' => 50,        // Limite de subdominios
            'limit_web_aliasdomain' => 16,    // Limite de alias entre dominios: domain.com --> domain.tld
            'limit_web_quota' => 20480,        // 20GiB Cuota de disco en MB
            'limit_traffic_quota' => -1,        // -1TiB Trafico web mensual en MB
            'force_suexec' => 'y',            // Forzado suexec para aumentar seguridad https://wiki.archlinux.org/index.php/Apache,_suEXEC_and_Virtual_Hosts
            'limit_hterror' => 'y',            // Documentos error personalizados
            'limit_ssl' => 'y',                // Permite el uso del protocolo HTTPs
            'limit_ssl_letsencrypt' => 'n',    // Cambiar a yes en entorno real
            'limit_wildcard' => 'y',            // Certificado SSL con wildcard para subdominios.
            'web_php_options' => 'no,fast-cgi,cgi,mod,suphp,php-fpm,hhvm',
            'limit_cgi' => 'y',                // Soporte CGI
            'limit_ssi' => 'y',                // Soporte SSI (Server Side Includes)
            'limit_perl' => 'y',                // Soporte Perl (Server Side Includes)
            'limit_ruby' => 'y',                // Soporte Ruby
            'limit_python' => 'y',            // Soporte Python
            'limit_ftp_user' => 20,            // Usuarios FTP
            'limit_shell_user' => 10,            // Usuarios SSH
            'ssh_chroot' => 'jailkit',        // Jaula acceso SSH (no, jailkit)
            'limit_webdav_user' => 20,        // Ususarios WebDav (permite montar unidades de red)
            'limit_backup' => 'y',            // Permite backups
            'limit_aps' => '12',                // Limite de aplicaciones empaquetadas (APS): wordpress, prestashop...

            // Servicio MySQL					// 5 x 2GiB database
            'limit_database' => 5,            // Limite de bases de datos
            'limit_database_user' => 30,        // Limite de usuarios mysql
            'limit_database_quota' => 2048,    // Cuota de disco reservada por base de datos

            // Parametros del Servicio Mail
            'limit_maildomain' => 1,            // Limite de dominios
            // Limitaciones por dominio
            'limit_mailquota' => 7680,        // 7,5GiB Cuota de disco reservado mail (independiente de otras cuotas)
            'limit_mailbox' => 15,            // 15 Buzones de correo de 512MiB
            'limit_mailalias' => 50,            // Alias email (info@midomino.com --> soporte@midomino.com)
            'limit_mailcatchall' => 1,        // Alias recoge-todo (catchall@midomino.com)
            'limit_mailaliasdomain' => 16,    // Alias de dominio (midominio.com <--> midominio.es).
            'limit_mailforward' => 20,        // Reenvios servidor externo: user@domain.tld -> user@gmail.com
            'limit_fetchmail' => 20,            // Download servidor externo: user@gmail.com -> user@domain.tld
            // No se limita el uso de listas negras o filtros spam a ningun tipo de usuario.
            'limit_mailfilter' => -1,
            'limit_spamfilter_wblist' => -1,
            'limit_spamfilter_user' => -1,
            'limit_spamfilter_policy' => -1
        );
    case "unlimited":
        $reseller_id = 0; // this id has to be 0 if the client shall not be assigned to admin or if the client is a reseller
        $account_params = array(
            // datos personales
            'company_name' => $_POST['companyname'],
            'contact_name' => $_POST['contactname'],
            'customer_no' => $_POST['customernumber'],
            'street' => $_POST['street'],
            'zip' => $_POST['zip'],
            'city' => $_POST['city'],
            'state' => $_POST['province'],
            'country' => $_POST['country'],
            'telephone' => $_POST['telephone'],
            'mobile' => '',
            'fax' => '',
            'email' => $_POST['email'],
            'internet' => '',
            'bank_account_owner' => $_POST['cc_name'],
            'bank_account_number' => $_POST['cc_number'],
            'paypal_email' => $_POST['paypal'],
            'username' => $_POST['username'],
            'password' => $_POST['password'],
            'language' => 'es',
            'usertheme' => 'default',
            'template_master' => 0,
            'template_additional' => '',
            'created_at' => time(),
            'added_by' => 'remote_api',

            // Parametros entorno multi-server
            // ID del servidor dns
            'default_dnsserver' => 1,
            'dns_servers' => 1,
            // ID del servidor web
            'default_webserver' => 1,
            'web_servers' => 1,
            'limit_web_ip' => '',
            // ID del servidor MySQL
            'default_dbserver' => 1,
            'db_servers' => 1,
            // ID del servidor mail
            'default_mailserver' => 1,
            'mail_servers' => 1,

            // Servicio DNS
            'limit_dns_zone' => 1,
            'limit_dns_slave_zone' => 0,
            'limit_dns_record' => 200,

            // Servicio Web
            'limit_web_domain' => 1,            // Limite de dominios
            // Limitaciones por dominio
            'limit_web_subdomain' => 100,        // Limite de subdominios
            'limit_web_aliasdomain' => 32,    // Limite de alias entre dominios: domain.com --> domain.tld
            'limit_web_quota' => 102400,        // 100GiB Cuota de disco en MB
            'limit_traffic_quota' => -1,        // Unlimited Trafico web mensual en MB
            'force_suexec' => 'y',            // Forzado suexec para aumentar seguridad https://wiki.archlinux.org/index.php/Apache,_suEXEC_and_Virtual_Hosts
            'limit_hterror' => 'y',            // Documentos error personalizados
            'limit_ssl' => 'y',                // Permite el uso del protocolo HTTPs
            'limit_ssl_letsencrypt' => 'n',    // Cambiar a yes en entorno real
            'limit_wildcard' => 'y',            // Certificado SSL con wildcard para subdominios.
            'web_php_options' => 'no,fast-cgi,cgi,mod,suphp,php-fpm,hhvm',
            'limit_cgi' => 'y',                // Soporte CGI
            'limit_ssi' => 'y',                // Soporte SSI (Server Side Includes)
            'limit_perl' => 'y',                // Soporte Perl (Server Side Includes)
            'limit_ruby' => 'y',                // Soporte Ruby
            'limit_python' => 'y',            // Soporte Python
            'limit_ftp_user' => 100,            // Usuarios FTP
            'limit_shell_user' => 50,            // Usuarios SSH
            'ssh_chroot' => 'jailkit',        // Jaula acceso SSH (no, jailkit)
            'limit_webdav_user' => 50,        // Ususarios WebDav (permite montar unidades de red)
            'limit_backup' => 'y',            // Permite backups
            'limit_aps' => '40',                // Limite de aplicaciones empaquetadas (APS): wordpress, prestashop...

            // Servicio MySQL					// 8 x 10GiB database
            'limit_database' => 8,            // Limite de bases de datos
            'limit_database_user' => 50,        // Limite de usuarios mysql
            'limit_database_quota' => 10240,    // Cuota de disco reservada por base de datos

            // Parametros del Servicio Mail
            'limit_maildomain' => 1,            // Limite de dominios
            // Limitaciones por dominio
            'limit_mailquota' => 40960,        // 40GiB Cuota de disco reservado mail a repartir entre los buzones
            'limit_mailbox' => 40,            // 40 Buzones de correo de 1024MiB
            'limit_mailalias' => 100,            // Alias email (info@midomino.com --> soporte@midomino.com)
            'limit_mailcatchall' => 5,        // Alias recogetodo (catchall@midomino.com)
            'limit_mailaliasdomain' => 32,    // Alias de dominio (midominio.com <--> midominio.es).
            'limit_mailforward' => 70,        // Reenvios servidor externo: user@domain.tld -> user@gmail.com
            'limit_fetchmail' => 70,            // Download servidor externo: user@gmail.com -> user@domain.tld
            // No se limita el uso de listas negras o filtros spam a ningun tipo de usuario.
            'limit_mailfilter' => -1,
            'limit_spamfilter_wblist' => -1,
            'limit_spamfilter_user' => -1,
            'limit_spamfilter_policy' => -1
        );
}