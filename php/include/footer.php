<!-- ::::::::::::::::::: Footer Area Start ::::::::::::::::::: -->
<footer class="footer_area">
    <div class="container">
        <div class="row">
            <!-- Footer About Area Start -->
            <div class="col-xs-12 col-sm-6 col-md-3">
                <div class="footer_about_us wow fadeInUp" data-wow-delay="0.2s">
                    <div class="title">
                        <h4>Acerca <span>de</span></h4>
                    </div>
                    <p>miHost es el nombre de una empresa de hosting ficticia.<br> Creada para el proyecto final.<br>Administración
                        de Sistemas Informáticos en Red<br>Curso: 2016-2017<br>I.E.S Maria Enríquez</p>
                    <div class="footer_social_area">
                        <a href="#"><i class="fa fa-facebook" aria-hidden="true"></i></a>
                        <a href="#"><i class="fa fa-twitter" aria-hidden="true"></i></a>
                        <a href="#"><i class="fa fa-skype" aria-hidden="true"></i></a>
                        <a href="#"><i class="fa fa-linkedin" aria-hidden="true"></i></a>
                        <a href="#"><i class="fa fa-pinterest" aria-hidden="true"></i></a>
                    </div>
                </div>
            </div>
            <!-- Footer About Area End -->

            <!-- latest news Area Start -->
            <div class="col-xs-12 col-sm-6 col-md-3">
                <div class="footer_upcoming_event wow fadeInUp" data-wow-delay="0.4s">
                    <div class="title">
                        <h4>Latest <span>News</span></h4>
                    </div>
                    <!-- Single latest post -->
                    <div class="event_single_post">
                        <img src="img/news-img/blog.jpg" alt="">
                        <a href="#">
                            <p>Como crear un nuevo buzón de correo?</p>
                        </a>
                    </div>
                    <!-- Single latest post -->
                    <div class="event_single_post">
                        <img src="img/news-img/blog-2.jpg" alt="">
                        <a href="#">
                            <p>¿Que es y como utilizar 1-Click CMS?</p>
                        </a>
                    </div>
                </div>
            </div>
            <!-- latest news Area End -->

            <!-- Contact info Area Start -->
            <div class="col-xs-12 col-sm-6 col-md-3">
                <div class="footer_contact_info wow fadeInUp" data-wow-delay="0.6s">
                    <div class="title">
                        <h4><span>Contacta</span> con nosotros</h4>
                    </div>
                    <!-- single contact info start -->
                    <div class="footer_single_contact_info">
                        <i class="fa fa-globe" aria-hidden="true"></i>
                        <p>Gandia, Valencia, España</p>
                    </div>
                    <!-- single contact info start -->
                    <div class="footer_single_contact_info">
                        <i class="fa fa-phone-square" aria-hidden="true"></i>
                        <a href="tel:+34611222333">+34 611 222 333</a>
                        <a href="tel:+34644555666">+34 644 555 666</a>
                    </div>
                    <!-- single contact info start -->
                    <div class="footer_single_contact_info">
                        <i class="fa fa-envelope-square" aria-hidden="true"></i>
                        <a href="mailto:info@educamp.com">info@mihost.ml</a>
                        <a href="mailto:care@educamp.com">soporte@mihost.ml</a>
                    </div>
                </div>
            </div>
            <!-- Contact info Area End -->

            <!-- Useful Links Area Start -->
            <div class="col-xs-12 col-sm-6 col-md-3">
                <div class="important_links wow fadeInUp" data-wow-delay="0.8s">
                    <div class="title">
                        <h4>Algunos <span>links</span></h4>
                    </div>
                    <!-- Links Start -->
                    <div class="links">
                        <p><i class="fa fa-angle-double-right" aria-hidden="true"></i>
                            <a href="pricing-plan.html">Nuestros productos</a></p>
                        <p><i class="fa fa-angle-double-right" aria-hidden="true"></i>
                            <a href="about-us.html">Conocenos</a></p>
                        <p><i class="fa fa-angle-double-right" aria-hidden="true"></i>
                            <a href="blog-sidebar.html">Nuestro blog</a></p>
                        <p><i class="fa fa-angle-double-right" aria-hidden="true"></i>
                            <a href="support.html">Soporte al cliente</a></p>
                        <p><i class="fa fa-angle-double-right" aria-hidden="true"></i>
                            <a href="contact.html">Trabaja en miHost</a></p>
                        <p><i class="fa fa-angle-double-right" aria-hidden="true"></i>
                            <a href="privacy-policy.html">Términos y condiciones</a></p>
                        <p><i class="fa fa-angle-double-right" aria-hidden="true"></i>
                            <a href="">Nuestro foro oficial</a></p>
                    </div>
                    <!-- Links End -->
                </div>
            </div>
            <!-- Useful Links Area End -->
        </div>
        <!-- end./ row -->
    </div>
    <!-- end./ container -->

    <!-- Bottom Footer Area Start -->
    <div class="footer_bottom_area">
        <div class="container">
            <div class="row">
                <div class="col-xs-12">
                    <div class="copywrite_text">
                        <p>Made with <i class="fa fa-heart" aria-hidden="true"></i> by <a href="#">miHost, Inc</a></p>
                    </div>
                    <!-- Bottom Footer Copywrite Text Area End -->
                </div>
            </div>
            <!-- end./ row -->
        </div>
        <!-- end./ container -->
    </div>
    <!-- Bottom Footer Area End -->

</footer>
<!-- ::::::::::::::::::: Footer Area End ::::::::::::::::::: -->

<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

