<?php

switch ($_SERVER['QUERY_STRING']) {
    case 'destroy':
        session_start();
        session_destroy();
        header('Location: ' . $_SERVER['HTTP_REFERER']);
        break;
    case 'newdomain':
        Session_start();
        if (isset($_POST['cartdomain'])) {
            $_SESSION['cart']['domain'] = $_POST['cartdomain'];
            header('Location: ../index.php#summary');
        } else {
            header('Location: ' . $_SERVER['HTTP_REFERER']);
        }
        break;
    case 'newplan':
        Session_start();
        if (isset($_POST['cartplan'])) {
            $_SESSION['cart']['plan'] = $_POST['cartplan'];
            header('Location: ../index.php#summary');
        } else {
            header('Location: ' . $_SERVER['HTTP_REFERER']);
        }
        break;
    default :
        header('Location: ' . $_SERVER['HTTP_REFERER']);


}







